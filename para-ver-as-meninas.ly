\version "2.22.0"
\include "empty-staff.ly"

% Colocar um staff vazio abaixo da melodia para rascunhar contracantos
usarRascunho = ##f

\header {
  title = "Para ver as meninas"
  composer = "Paulinho da viola"
  tagline = \markup \teeny {
    "Editado com" \italic "lilypond" \normal-text
    "por João Seckler em set/2022" }
}

staffglobal = {
  \key a \minor
  \time 2/4
  % \tempo 4 = 80
  \set Timing.beamExceptions = #'()
  \set Timing.beatStructure = 1,1,1,1
}

acordes = \chordmode {
  \set majorSevenSymbol = \markup { maj7 }
  \partial 8. s8.
  a2:m |
  c:7/g |
  b:7/fis |
  s |
  b:m7.5- |
  e:7 |
  a:m |
  a:7 |
  e:m7.5-/bes |
  a:7 |
  d:m7 |
  s |
  g:7 |
  s |
  c:maj7 |
  b4:m7.5- e:7 |
  a2:7 |
  s |
  \compressMMRests {
    e:m7.5-/bes |
    R2*1000 |
    s2 |
  }
  a2:7 |
  d:m7 |
  s |
  b:m7.5- |
  e:7 |
  a:m |
  s |
  c:7/g |
  s |
  f:7 |
  e:7 |
  a:m |
  b:m7.5- |
  a:m/c |
  g:7 |
  c:maj7 |
  a:7 |
  d:m7 |
  e:7 |
  a:m |
  a:7 |
  d:m7 |
  b:m7.5- |
  a:m |
  d4:m7 e:7 |
  a2:m

}

melodia = \relative c'' {
  \partial 8. e8 c16~ |
  c4. b8 |
  d4. c16 b~ |
  b4.. b16 |
  c d c b ais b8 f'16~ |
  f2~ |
  f4 r16 c8 e16~ |
  e8. e16 r8. e16 |
  f g f e dis e8 g16~ |
  g2~ |
  g4 r16 f8 a16~ |
  a8. a16 r8. d,16 |
  e f e d~ d f8.~ |
  f2 |
  r16 d e f e d8 g16~ |
  g2 |
  r4 a16 g8 a16~ |
  a2 |
  e4.. g16~ |
  g8. g16 r4 |
  R2*1000
  r4.. a16 |
  bes16 a8 g32 f e16 g8 f16~ |
  f8. f16~ f4 |
  r16 d e f e d8 f16~|
  f8. f16~ f4 |
  r16 a g f e f8 e16~ |
  e8. e16~ e4 |
  r8. c16 d e8 e16~ |
  e4.. c16 |
  d16 e8 d16~ d16 c8 a'16~ |
  a2 |
  gis2 |
  r8. e16 a a a gis~ |
  gis8. f16 e8 c~ |
  c8 c16 a a c8 e16 |
  g8. f16~ f16 e8 g16~ |
  g4 r16 e16 e f |
  g8. f16~ f e8 a16~ |
  a2 |
  b |
  r4. c16 c |
  b a8 g16~ g b8 a16~ |
  a4. a16 a |
  g f8 e16~ e d8 c16~ |
  c4 r16 e8 a16~ |
  a g f e b e8 b16~ |
  b8. a16~ a4 |
  r4 r16
}

letra = \lyricmode {
  Si -- lên -- cio por fa -- vor
  En -- quan -- to~es -- que -- ço~um pou -- co~a
  dor do pei -- to
  Não di -- ga na -- da
  So -- bre meus de -- fei -- tos
  Eu não me lem -- bro mais
  Quem me dei -- xou as -- sim

  Ho -- je~eu que -- ro~a -- pe -- nas
  U -- ma pau -- sa de mil com -- pas -- sos
  Pa -- ra ver as me -- ni -- nas
  E na -- da mais nos bra -- ços
  Só es -- te~a -- mor
  As -- sim des -- con -- tra -- í -- do

  Quem sa -- be de tu -- do não fa -- le
  Quem não sa -- be na -- da se cale
  Se for pre -- ci -- so~eu re -- pi -- to
  Por -- que ho -- je~eu vou fa -- zer
  Ao meu jei -- to~eu vou fa -- zer
  Um sam -- ba so -- bre~o in -- fi -- ni -- to
}

\paper {
  system-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 3)
       (stretchability . 12))
  markup-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 3)
       (stretchability . 12))
}

\layout {
  \context {
    \Lyrics
      \override VerticalAxisGroup.
        nonstaff-relatedstaff-spacing.padding = #1.5
      \override VerticalAxisGroup.
        nonstaff-unrelatedstaff-spacing.padding = #1.5
      \override LyricSpace.minimum-distance = #1.0
  }
  \context {
    \ChordNames
      \override VerticalAxisGroup.
        nonstaff-relatedstaff-spacing.padding = #2
      \override VerticalAxisGroup.
        nonstaff-unrelatedstaff-spacing.padding = #2
    \override LyricSpace.minimum-distance = #1.0
  }
}

\score {
  <<
    \new ChordNames \acordes

    \new Staff {
      \staffglobal
      \new Voice = "melodia" \melodia
    }
    \new Lyrics \lyricsto "melodia" \letra
    $(if usarRascunho
    #{
    \new Staff {
      \staffglobal
      \new Voice = "rascunho" \empty \melodia
    }
    #}
    #{ #}
    )
  >>
  \layout {}
}


