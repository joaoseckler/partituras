\version "2.22.0"
\include "sinc.ly"

\header {
  title = "Minha missão"
  composer = "João nogueira"
  tagline = \markup \teeny {
    "Editado com" \italic "lilypond" \normal-text
     "por João Seckler em nov/2022"
   }
}

tom = d' % bom pra flauta, tenor, violão
% tom = a' % bom pro violão
% tom = b  % original (bom pra barítono, voz no geral)

staffglobal = {
  \key b \minor
  \time 2/4
  % \tempo 4 = 80
  \set Timing.beamExceptions = #'()
  \set Timing.beatStructure = 1,1,1,1
}

DCfine = {
  \once \override Score.RehearsalMark.break-visibility = #'#(#t #t #f)
  \once \override Score.RehearsalMark.direction = #DOWN
  \mark \markup { \small "D.C. al fine" }
}

acordes = \chordmode {
  \set majorSevenSymbol = \markup { maj7 }
  \repeat volta 2 {
    b2:m | s |
    cis:m7.5- | s |
    b:m | g:7 |
    fis:7 | s |
    b:m | g:7 |
    fis:7 | s |
  }
  \repeat volta 2 {
    b:7 | s
    e:7sus4 | e:7 |
    a:7 | s
    d:7sus4 | d:7 |
    g:7 | s
  }
  \alternative {
    {
      cis:m7.5- | s
      fis:7 | s |
      b:m | s |
    }
    {
      cis:m7.5- | c:7 |
      b:m | cis4:m7.5- fis4:7 |
      b2:m | fis:7 |
    }
  }

  b:m | s |
  a | s |
  g | fis:7 |
  b:m | fis4:m7.5- b:7 |
  e2:m | cis:m7.5- |
  b:m | s |
  cis:7 | s |
  fis:7 | s |

  \repeat volta 2 {
    b:m | s |
    a | s |
    g | fis:7 |
    b:m | fis4:m7.5- b:7 |
    e2:m | cis:m7.5- |
    b:m | d:7/a |
    g:7 | fis:7 |
    b:m |
  }

  \alternative {
    { f:dim | }
    { fis:7 | }
  }

  b2:m | s |
  cis:m7.5- | fis:7 |
  b:m | g:7 |
  fis:7 | s |
  b:m | fis:7.5-/c |
  b:m | s |
  e:m | g:7 |
  fis:7 | s |
  g | fis:7 |


}

melodia = \transpose a b \relative c'' {
  \repeat volta 2 {
    a2 |
    c4.. b16~ |
    b8. b16 r8. a16 |
    g16 f8 c16~ c16 d8 e16~ |
    e2~ |
    e4 r16 a8 gis16~ |
    gis8. gis16 r8. f16 |
    e16 d8 a16~ a16 b8 c16~ |
    c2~ |
    c4 r16 f8 e16~ |
    e16 d8 b16~ b4~ |
    b2|
  }

  \repeat volta 2 {
    \sinc a cis e \sinc a, cis e |
    a,4 \sinc r a' g~ |
    g2 |
    fis4 r4 |
    \sinc g, b d \sinc g, b d |
    g,4 \sinc r f' f~ |
    f2 |
    e4 r4 |
    \sinc f, a c \sinc f, a c |
    f,4 \sinc r e' e~ |
  }
  \alternative {
    {
      e2 |
      d2 |
      \sinc b c d \sinc b c d
      b4 \sinc r d d~ |
      d2 |
      \once \override Voice.Accidental.parenthesized = ##t
      c!2 | \break
    }
    {
      e2 |
      d4 \sinc r e c'~ |
      \sinc c b a~ a16 g f e |
      d4 r16 e8. |
      a,2 |
      r2 \bar "||"
    }
  }

  \break

  a'4~ a8. c16~ |
  \sinc c b a~ \sinc a e f |
  g4~ g8. b16~ |
  \sinc b a g~ \sinc g d e |
  f4~ f8. a16~ |
  \sinc a g f~ \sinc f c d |
  e2 |
  r2 |
  d4~ \sinc d f e~ |
  \sinc e d a \sinc b c b~ |
  b16 a8.~ a4 |
  r2 |
  b4 \sinc dis fis a |
  c4 \sinc r b a~ |
  \sinc a gis b~ b4 |
  r2 |
  \break

  a4~ a8. c16~ |
  \sinc c b a~ \sinc a e f |
  g4~ g8. b16~ |
  \sinc b a g~ \sinc g d e |
  f4~ f8. a16~ |
  \sinc a g f~ \sinc f c d |
  e2 |
  r2 |
  \break

  \repeat volta 2 {
    d4 \sinc e f a |
    c4~ \sinc c b b~ |
    b4 \sinc a e c |
    g'4~ \sinc g ges f~ |
    f4 \sinc d b gis |
    c4~ \sinc c b a~ |
    a2 |
  }
  \alternative {
    { r2 | }
    { r2 \DCfine \bar "||" }
  }
  \break

  \repeat volta 2 {
    a'2 |
    c4.. b16~ |
    b8. b16 r8. a16 |
    g16 f8 c16~ c16 d8 e16~ |
    e2~ |
    e4 r16 a8 gis16~ |
    gis8. gis16 r8. f16 |
    e16 d8 a16~ a16 b8 c16~ |
    c16 c8. r4 |
    r2 |
    r4 \sinc r a b |
    \sinc c d e~ \sinc e f e~ |
    e4~ \sinc e d b~ |
    b4 gis |
    r4 \sinc r e' fis |
    \sinc gis a b~ \sinc b c b~ |
    b4~ \sinc b a a~ |
    a4 a |
  }
}

letra = \lyricmode {
  \override LyricText.self-alignment-X = #LEFT
  <<
    {
      Quan -- do~eu can -- to
      É pa -- ra~a -- li -- vi -- ar meu pran -- to
      E~o pran -- to de quem já
      Tan -- to so -- freu

      % repeat

      Can -- to pa -- ra~a -- nun -- ci -- ar o di -- a
      Can -- to pa -- ra~a -- me -- ni -- zar a noi -- te
      Can -- to pra de -- nun -- ci -- ar o açoi -- te
      Can -- to tam -- bém con -- tra~a ti -- ra -- ni -- a
    }
    \new Lyrics {
      \override LyricText.self-alignment-X = #LEFT
      Quan -- do~eu can -- to
      Es -- tou sen -- tin -- do~a luz de um san -- to
      Es -- tou ajo -- e -- lhando
      Aos pés de Deus

      % repeat

      Can -- to por -- que nu -- ma me -- lo -- di -- a
      A -- cen -- do no co -- ra -- ção do po -- vo
      A es -- pe -- ran -- ça de~um mun -- do
      % invisible character to make hyphen appear
    }
  >>

  no -- vo E~a lu -- ta pa -- ra se vi -- ver em paz!

  Do po -- der da cri -- a -- ção
  Sou con -- ti -- nu -- a -- ção
  E que -- ro~a -- gra -- de -- cer
  Foi ou -- vi -- da mi -- nha sú -- pli -- ca
  Men -- sa -- gei -- ro sou da mú -- si -- ca
  O meu can -- to~é~u -- ma mis -- são
  Tem for -- ça de~o -- ra -- ção
  E~eu cum -- pro~o meu de -- ver
  Aos que vi -- vem a cho -- rar
  Eu vi -- vo pra can -- tar
  E can -- to pra vi -- ver

  Quan -- do~eu can -- to
  a mor -- te me per -- corre
  E~eu sol -- to um can -- to da gar -- gan -- ta
  Que~a ci -- gar -- ra quan -- do can -- ta mor -- re
  E~a ma -- dei -- ra quan -- do mor -- re can -- ta!
}

\paper {
  system-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 3)
       (stretchability . 12))
  markup-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 3)
       (stretchability . 12))
}

\layout {
  \context {
    \Lyrics
      \override VerticalAxisGroup.
        nonstaff-relatedstaff-spacing.padding = #2
      \override VerticalAxisGroup.
        nonstaff-unrelatedstaff-spacing.padding = #2
      \override LyricSpace.minimum-distance = #1.5
  }
  \context {
    \ChordNames
      \override VerticalAxisGroup.
        nonstaff-relatedstaff-spacing.padding = #3
      \override VerticalAxisGroup.
        nonstaff-unrelatedstaff-spacing.padding = #3
    \override LyricSpace.minimum-distance = #1.5
  }
}

\score {
  \transpose b \tom {
    <<
      \new ChordNames = "chords" \acordes

      \new Staff {
        \staffglobal
        \new Voice = "melodia" \melodia
      }
      \new Lyrics = "lyrics" \lyricsto "melodia" \letra
      >>
  }
  \layout {}
}


