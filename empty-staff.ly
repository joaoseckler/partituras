\version "2.22.0"

% Make an empty version of music
empty =
#(define-music-function
   (music) (ly:music?)
   (define ret #{ s #})
   (ly:music-set-property!
     ret
     'duration
     (make-duration-of-length (ly:music-length music)))
   ret
   )

