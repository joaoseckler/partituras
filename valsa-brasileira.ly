\version "2.24.2"
\include "sinc.ly"

\header {
  title = "Valsa brasileira"
  composer = "Edu Lobo e Chico Buarque"
  tagline = ""
}

"\\-" = \tweak minimum-distance #1 #(make-music 'HyphenEvent)


\paper {
  system-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 3)
       (stretchability . 12))
  markup-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 3)
       (stretchability . 12))
}

\layout {
  \context {
    \Lyrics
      \override VerticalAxisGroup.
        nonstaff-relatedstaff-spacing.padding = #2
      \override VerticalAxisGroup.
        nonstaff-unrelatedstaff-spacing.padding = #2
      \override VerticalAxisGroup.
        nonstaff-nonstaff-spacing.padding = #1.25
      \override LyricSpace.minimum-distance = #1.0
  }
  \context {
    \ChordNames
      \override VerticalAxisGroup.
        nonstaff-relatedstaff-spacing.padding = #2
      \override VerticalAxisGroup.
        nonstaff-unrelatedstaff-spacing.padding = #2
    \override LyricSpace.minimum-distance = #1.0
  }
}

letra = \lyricmode {
  Vi -- vi -- a~a te bus
  <<
    {
      car Por -- que pen -- san -- do~em ti
      Cor -- ri -- a con -- tra~o tem -- po
      Eu des -- car -- ta -- va~os di -- as
      Em que não te vi
      Co -- mo de~um fil -- me~A~a -- ção que não va -- leu
      Ro -- da -- va~as ho -- ras pra trás
      Rou -- ba -- va~um pou -- qui -- nho
      E~a -- jei -- ta -- va~o meu ca -- mi -- nho
      Pra~en -- cos -- tar no teu
      Su -- bia na mon-
    }

    \new Lyrics {
      -ta -- nha
      Não co -- mo~an -- da~um cor -- po
      Mas um sen -- ti -- men -- to
      Eu sur -- preen -- di -- a~o sol
      An -- tes do sol rai -- ar
      Sal -- ta -- va~as noi -- tes
      Sem me re -- fa -- zer
      E pe -- la por -- ta de trás
      Da ca -- sa va -- zi -- a
      Eu in -- gres -- sa -- ri -- a

      \skip 1 * 3 * 3

      E te ve -- ri -- a
      Con -- fu -- sa por me ver
      Che -- gan -- do~as -- sim
      Mil di -- as an -- tes de te co -- nhe -- cer
    }
  >>
}

intro = \relative c'' {
  r8 aes g f es d |
  f es d es f g |
  r bes a g f e |
  g f e f g a | \break
  r c b a g fis |
  a g fis g b d |
  g2 d8 g |
  e2. | \break
  f2  f,8 f' |
  d2. |
  es2 des8 es |
  c << g es bes >> << f' d a >> << es' c g >> << d' bes f >> << c' aes es >> |
  es' d des c b c |
  es2. |
}

canto = \relative c' {
  <<
    \new Voice = "canto" {
      \voiceOne
      r8 f g aes es' b

      \oneVoice
      \repeat volta 2 {
        d8^\segno c bes aes g es |
        f g aes b f' es |
        d4. c8~ c4 | \break
        r8 des e g c bes |
        bes aes g aes bes c |
        c4. b8 bes e, |
        g fis f es c aes | \break
        g'4. e8 f aes |
        g f aes g~ g4~ |
        g f8 bes aes e |
        g2 f4 | \break
        es8 g, c es g c |

        \alternative {
          \volta 1 {
            b8 a fis d c a |
            d2.~ |
            d8 f, g aes es' b | \break
          }
          \volta 2 {
            r8 g c es g c |
            r8 g, b c es g |
            g2.~ |
            g4. f8 es c | \break
            c4.bes8 a bes |
            c^\markup { \coda } bes a bes c d \bar "||"
            es2
          }
        }
      }
    } \\ \new Voice { \voiceTwo d,2. }
  >>
}

outro = \relative c'' {
  d8 es |
  aes ges es ces bes ces |
  ces2 bes8 ces | \break
  es des ces aes g aes |
  bes4. bes8 ces bes~ |
  bes bes ces bes des bes |
  c!4. aes'8 c, fes |
  es4. d8 es f? | \break
  es4. es8 g, c |
  bes4. a8 des es |
  d!2.
  { r8 f, g aes^\markup { Ao \segno, casa 2 e \coda } es'8 b }
  \addlyrics { Su -- bi -- a na mon- }
  \once \override Score.TimeSignature.break-visibility = ##(#f #f #t)
  \break

  \bar "||"

  <<
    \new Voice = "letracoda" {
      \oneVoice
      \time 4/4
      \set Timing.beamExceptions = #'()
      \set Timing.beatStructure = 1,1,1,1
      c8^\markup { \coda } bes a bes c4 d |

      \time 3/4
      \set Timing.beamExceptions = #'()
      \set Timing.beatStructure = 1,1,1
      \voiceOne
      es2.

    } \\ \new Voice {
      \time 4/4
      \set Timing.beamExceptions = #'()
      \set Timing.beatStructure = 1,1,1,1
      s1 |

      \time 3/4
      \set Timing.beamExceptions = #'()
      \set Timing.beatStructure = 1,1,1
      \voiceTwo
      r8 des bes ces es, f |

      \oneVoice
      bes ces es, f bes, ces | \break
      g' aes c,! es g bes |
      bes ces aes a ces es~ |
      es2.~ |
      es2. \bar "|."
    }
  >>

}

chExceptionMusic = {
  <c es ges>1-\markup { \super "° 7M" }
  <c es g bes d'>1-\markup { m \super "7 9"}
  <c e g bes d'>1-\markup { \super "7 9"}
}

chExceptions = #(append
  (sequential-music-to-chord-exceptions chExceptionMusic #t)
  ignatzekExceptions)

acordes = {
  \chordmode {
    g2.:7.9- |
    c:m7.9 |
    cis:dim7 |
    d:m7.9 |
    dis:dim7 |
    c:maj7/e |
    g:/f |
    c:/e |
    f:/es |
    bes:/d |
    es:/des |
    c:m |
    a2:m7.5- d4:7.13.9- |
    g2.:maj7.5+ |
    g:7.9- |
    c:m7 |
    g:7.9- |
    c:m7 |
    cis:dim7 |
    aes:/c |
  }
  { <des fes aeses> }

  \chordmode {
    aes:/c |
    b:dim7 |
    es:7/bes |
    a:7.11+ |
    aes:7+.5+ |
    a:m7.5- |
    d:7.9-.13 |
    aes:7.9.11+ |
    g:7.9- |
    c:m7/bes |
    c:m7/b |
    aes:maj7/c |
    f2:7.9 fis4:dim7 |
    c2.:m/g

    bes:7.9 |
    ces:/ges |
    ces:/ges |
    f:m7.5- |
    f:m7.5- |
    des:m6/fes |
    es:7.9- |
    aes:maj7.5+ |
    g:7.9- |
    c2:m7 c4:m/bes |
    a2:m7.5- d4:7.9-.13- |
    g2.:maj7 |
    g:7.9- |

    bes1:7.9 |
    ces2.:7.13 |
    aes:m6.9.11 |
    f:m7.9 |
    e2.:maj7 |
    es
  }

}

\score {
  \transpose c c {
    <<
      \new ChordNames {
        \set chordNameExceptions = #chExceptions
        \set majorSevenSymbol = "7M"
        \set chordChanges = ##t
        \acordes
      }

      \new Staff {
        \key c \minor
        \time 3/4
        \set Timing.beamExceptions = #'()
        \set Timing.beatStructure = 1,1,1

        \new Voice \intro \canto \outro
      }
      \new Lyrics = "letra" \lyricsto "canto" \letra
      \new Lyrics \lyricsto "letracoda" { an -- tes de te co -- nhe -- cer }
    >>
  }
}


