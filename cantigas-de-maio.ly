\version "2.20.0"
\include "defaults.ly"

\paper { % returning to defaults, to fit in one page
  system-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 1)
       (stretchability . 60))
  markup-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 2)
       (stretchability . 60))
}

\header {
  title = "Cantigas de maio"
  composer = "José Afonso"
}

accel = \markup \tiny \italic \bold "accel..."
ritar = \markup \tiny \italic \bold "ritar..."

staffglobal = {
  \key f \minor
  \time 4/4
  % { \partial 4 }
  % \set Timing.beamExceptions = #'()
  % \set Timing.beatStructure = 1,1,1,1

}

frase_a = \relative c' {
  % f8 f | % partial
  f2~ f8 c' c c |
  c4. bes8 bes4 c |
  aes4 bes g4. e8 |
  f2.
}

frase_b = \relative c'' {
  aes8 aes |
  aes2~ aes8 g aes bes |
  c4.( bes8) aes4 g8 f |
  c4 f g4. e8 |
  % f2. % not here because of volta
}

refrao = \relative c'' {
  c4 des8 es4 des8 c des |
  c1 |
  c4 des8 es4 des8 c des |
  c4. es8 es4 des8 c~ |
  c4 bes8 c4 c bes8 |
  aes4( f4.) aes8 aes bes |
  c4 bes8 c4 c bes8 |
  aes4( f2.)


  c'4 des8 es4 des8 c des |
  c1 |
  c4 des8 es4 des8 c des |
  c4. es4 es des8 |
  c4 bes8 c4 c bes8 |
  aes4( f4.) aes8 aes bes |
  c4 bes8 c4 c bes8 |
  aes4( f4.) aes8^\ritar aes bes |
  c4 bes8 c4 c bes8 |
  aes4( f2.) |
  r1_"Da Capo" |

}


\score {
  <<
    \new ChordNames \chordmode {
      % \set chordChanges = ##t % We actually don't want this this time!
      \repeat unfold 2 { c1:m | f:m | }
      \repeat unfold 2 {
        f2:m f2:m/es |
        bes:m es |
        bes:m/des c:7 |
        f1:m
      }
      \repeat unfold 2 {
        f1:m |
        aes |
        c:7 |
        f1:m |
      }
      f1:m |

      \repeat unfold 4 { f1:m | }
      aes |
      f:m
      aes |
      f:m |

      \repeat unfold 4 { f1:m | }
      c:m |
      f:m
      c:m |
      f:m |
      c:m |
      f:m |
      c:m |
    }

    \new Staff {
      \staffglobal
      \new Voice = "intro" \relative c'' {
        \set fontSize = #-2
        g4 f8 c~ c des es des |
        c1 |
        g'4 f8 c~ c des es des |
        c2.
      }

      \new Voice = "melodia" \relative c' {
        f8 f |
        \repeat volta 2 {
          \frase_a
          f8 f \frase_a
          \frase_b
          f2. \frase_b
        }
        \alternative {
          \relative c'{ f2. f8 f }
          \relative c'{ f1^\accel }
        }
      }
      \new Voice = "refrao" {
        \refrao
      }
      \bar "||"
    }

    \new Lyrics \lyricsto "melodia" {

      eu fui ver a mi -- nha_a -- ma -- da
      lá prós bai -- xos dum jar -- dim
      eu fui ver a mi -- nha_a -- ma -- da
      lá prós bai -- xos dum jar -- dim
      dei\-lhe_u -- ma ro -- sa_en -- car -- na -- da
      pa -- ra se lem -- brar de mim
      dei\-lhe_u -- ma ro -- sa_en -- car -- na -- da
      pa -- ra se lem -- brar de mim

      eu fui % disgusting ...
    }

    \new Lyrics \lyricsto "melodia" {
      _ _ ver o meu ben -- zi -- nho % ...
      lá prós lados dum pa -- çal
      eu fui ver o meu ben -- zi -- nho
      lá prós lados dum pa -- çal
      dei\-lhe o meu len -- ço de li -- nho
      que_é do mais fi -- no bra -- gal
      dei\-lhe o meu len -- ço de li -- nho
      que_é do mais fi -- no bra gal

    }

    \new Lyrics \lyricsto "refrao" {
      mi -- nha mãe quan -- do_eu mor -- rer
      mi -- nha mãe quan -- do_eu mor -- rer
      ai cho -- re por quem mui -- to_a -- mar -- gou
      ai cho -- re por quem mui -- to_a -- mar -- gou

      pa -- ra_en -- tão di -- zer ao mundo
      pa -- ra_en -- tão di -- zer ao mundo

      ai Deus mo deu ai Deus mo le -- vou
      ai Deus mo deu ai Deus mo le -- vou
      ai Deus mo deu ai Deus mo le -- vou
    }

  >>
}

\markup {
  \hspace #20
  \column {
    \line { "eu fui ver uma donzela" }
    \line { "numa barquinha a dormir" }
    \line { "dei-lhe uma colcha de seda" }
    \line { "para nela se cobrir" }
    \line { " " } % ugh....
    \line { "eu fui ver uma solteira" }
    \line { "numa salinha a fiar" }
    \line { "dei-lhe uma rosa vermelha" }
    \line { "para de mim se encantar" }
    \line { " " }
    \line { "eu fui ver a minha amada" }
    \line { "lá nos campos eu fui ver" }
    \line { "dei-lhe uma rosa encarnada" }
    \line { "para de mim se prender" }
    \line { " " }
    \line { "verdes prados verdes campos" }
    \line { "onde está minha paixão" }
    \line { "as andorinhas não param" }
    \line { "umas voltam outras não" }
  }
}

