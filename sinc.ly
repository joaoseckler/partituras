\version "2.22.0"

set-duration = #(define-music-function (music n) (ly:music? ly:duration?)
 (ly:music-set-property! music 'duration n)
 music
)

% Create sincopation
sinc =
#(define-music-function (n1 n2 n3)
                        (ly:music? ly:music? ly:music?)
                        #{
                        $(set-duration n1 #{ 16 #})
                        $(set-duration n2 #{ 8 #})
                        $(set-duration n3 #{ 16 #})

                        #})

