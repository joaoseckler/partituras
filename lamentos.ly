\version "2.20.0"
\include "sinc.ly"

\header {
  title = "Lamentos"
  subtitle = "Segunda voz (A') versão Jacob"
  composer = "Pixinguinha"
}

\paper {
  system-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 3)
       (stretchability . 12))
  markup-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 3)
       (stretchability . 12))
}

\layout {
  \context {
    \ChordNames
      \override VerticalAxisGroup.
        nonstaff-relatedstaff-spacing.padding = #2
      \override VerticalAxisGroup.
        nonstaff-unrelatedstaff-spacing.padding = #2
    \override LyricSpace.minimum-distance = #1.0
  }
}


staffglobal = {
  \key d \major
  \time 2/4
  % \tempo 4 = 80
  \set Timing.beamExceptions = #'()
  \set Timing.beatStructure = 1,1
}

\score {
  <<
    \new Staff {
      \staffglobal
      \new Voice = "melodia" \relative c'' {
        \partial 4 fis4 \bar "||"
        e2 |
        d4. dis,8 |
        cis'2 |
        b4.
        \once \override Voice.Accidental.parenthesized = ##t
        bes8 |
        a4 b |
        cis~ cis16 d8 g16~ |
        g2 |
        fis4~ \sinc fis g fis~ |
        \sinc fis e g~ g4~ |
        g \sinc r cis, e~ |
        \sinc e d fis~ fis4~ \bar ""
        % fis4 \sinc r d cis~ |
        % \sinc cis ais cis~ \sinc cis fis dis~ |
        % dis4 \sinc r eis gis~ |
        % \sinc gis fis a~ \sinc a g! dis |
        % \sinc fis e d! \sinc cis b a |
        % d2 |
        % \sinc r b c \sinc d e fis |
        % a4~ \sinc a g fis~ |
        % fis4~ \sinc fis e e~ |
        % e8. d16 \sinc fis, a d |
        % b8. d16 cis8. e16 |
        % d2
      }
    }
    \new Staff {
      \staffglobal
      \new Voice = "melodia2" \relative c'' {
        \partial 4 a4 \bar "||"
        g2 |
        fis4. a,8 |
        f'2 |
        aes4. g8 |
        fis4 g |
        a~ \sinc a ais b~ |
        b2 |
        a4~ \sinc a b a~ |
        \sinc a g b~ b4~ |
        b4 \sinc r ais cis~ |
        \sinc cis b d~ d4 \bar ""

      }
    }
  >>
  \layout {}
}

