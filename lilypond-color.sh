#!/bin/bash

RED='\033[1;31m'
YELLOW='\033[1;33m'
NC='\033[0m' # No Color

CUR="$NC"

while IFS= read line; do
  if echo "$line" | grep -iq 'warning'; then
    CUR="${YELLOW}"
  elif echo "$line" | grep -iq 'error'; then
    CUR="${RED}"
  elif echo "$line" | grep -Pvq '^ '; then
    CUR="${NC}"
  fi
  echo -e "${CUR}${line}${NC}"
done < /dev/stdin

