\version "2.24.4"
\include "sinc.ly"


\paper {
  system-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 3)
       (stretchability . 12))
  markup-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 3)
       (stretchability . 12))
}

\layout {
  \context {
    \ChordNames
      \override VerticalAxisGroup.
        nonstaff-relatedstaff-spacing.padding = #2
      \override VerticalAxisGroup.
        nonstaff-unrelatedstaff-spacing.padding = #2
    \override LyricSpace.minimum-distance = #1.0
  }
}


staffglobal = {
  \key g \minor
  \time 2/4
  % \tempo 4 = 80
  \set Timing.beamExceptions = #'()
  \set Timing.beatStructure = 1,1
}

\book {
  \header {
    title = "Cuidado violão"
    subtitle = "Vozes violões"
    composer = "José Toledo"
    tagline = ""
  }

  \score {
    <<
      \new Staff {
        \staffglobal
        \new Voice \relative c' {
          \clef "treble_8"
          \partial 16*5 a16 bes a g f |
          es8. f16 g f es d |
          c d es d c bes a g |
          fis16 r4..
        }
      }
      \new Staff {
        \staffglobal
        \new Voice \relative c {
          \clef bass
          \partial 16*5 f16 g f es d
          c8. d16 es d c bes |
          a bes c bes a g fis es |
          d r16 r4.
        }
      }
    >>
  }

  \score {
    <<
      \new Staff {
        \staffglobal
        \new Voice \relative c' {
          \clef "treble_8"
          \partial 16*5 a'16 g f es d |
          g8. f16 es d c bes |
          es d c bes a g fis es |
          d e fis aes bes c d e |
          fis8
          \footnote #'(-1 . 3) "Penezzi e outros fazem esse mi bemol" e16
          d c bes a aes |
          g2
        }
      }
      \new Staff {
        \staffglobal
        \new Voice \relative c {
          \clef bass
          \partial 16*5 f'16 es d c bes |
          es8. d16 c bes a g |
          c bes a g fis es d c |
          bes c d e fis aes bes c |
          d8 c16 bes a g fis es |
          d2
        }
      }
    >>
  }
}
