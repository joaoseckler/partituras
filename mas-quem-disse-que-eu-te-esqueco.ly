\version "2.24.3"
\include "defaults.ly"
\include "sinc.ly"

\header {
  title = "Mas quem disse que eu te esqueço"
  composer = "Dona Ivone Lara e Hermínio Bello de Carvalho"
  arranger = "(versão da Beth Carvalho)"
}

staffglobal = {
  \key a \major
  \time 2/4
  \clef "treble_8"
  \set Timing.beamExceptions = #'()
  \set Timing.beatStructure = 1,1,1,1

}

melodia = \relative c' {
  \partial 16 cis16 |
  \repeat volta 2 {
    \sinc e fis a \sinc fis e cis |
    \sinc b a fis \sinc a fis a |
    cis2 |
    \sinc cis b cis b8. fis16 |
    cis'2 |
    \sinc cis b cis e8. cis16 |
    \alternative {
      { fis,2 | r4 r8. cis'16 }
      { a2 }
    }
  }
  r4 \sinc r e' e~ \bar "||"
  e16 d cis cis \sinc b a cis~ |
  cis16 cis8. r8. gis16 |
  a8 fis16 gis \sinc a cis gis~ |
  gis16 fis8. r8. d'16~ |
  \sinc d cis b b16 a gis b~ |
  b4 r8. a16~ |
  a fis cis e \sinc fis a fis~ |
  fis fis8. \sinc r a a~ |

  \sinc a cis, cis~ cis16 e gis fis~ |
  fis fis8. \sinc r b b~ |
  \sinc b dis, fis~ fis16 a cis gis~ |
  gis4 \sinc r fis d'~ |
  \sinc d d d~ d16 d cis e~ |
  e8. e16 \sinc r16 e, a~ |
  \sinc a b cis~ cis a fis a~ |

  a4 \sinc r a^coro a~
  \sinc a a a~ a16 a a8 |
  gis8. fis16~ \sinc fis b b~ |
  \sinc b dis, fis~ fis16 a cis gis~ |
  gis4 \sinc r fis d'~ |
  \sinc d d d~ d16 d cis e~ |
  e8. e16 \sinc r16 e, a~ |
  \sinc a b cis~ cis a fis a~
  a4 \sinc r cis^solo gis'~ \bar "||"

  \sinc gis e fis~ fis4 |
  r8. e16~ \sinc e d cis~ |
  cis8. fis,16~ fis r fis16^coro fis |
  \sinc b b cis~ \sinc cis cis gis~ |
  gis8. fis16~ fis r fis fis
  \sinc b b cis~ \sinc cis gis fis~ |
  fis8. e16~ e4 |
  r4 \sinc r cis'^solo gis'~ |

  \sinc gis e fis~ \sinc fis fis a~ |
  a8. fis16~ fis8 e~ |
  e16 fis,8. \sinc r fis^coro fis |
  \sinc b b cis~ \sinc cis cis e~ |
  e8. b16~ \sinc b16 cis cis |
  \sinc fis, fis cis'~ \sinc cis cis cis~ |
  cis8. a16~ a4 |
  r2
}

letra = \lyricmode {
  la -- ia -- ra -- ra -- ia -- ra -- ra -- ia -- ra -- ra -- ia -- ra -- ra -- ia
  la -- ra -- ra -- ia -- ra -- ia
  la -- ra -- ra -- ia -- ra -- ia

  % levare da volta
  la

  % casa 2
  ia

  tris -- te -- za ro -- lou dos meus o -- lhos
  de~um jei -- to que~eu não que -- ri -- a
  e man -- chou meu co -- ra -- ção
  que ta -- ma -- nha co -- var -- di -- a

  a -- fi -- ve -- la -- ram meu pei -- to
  pra eu dei -- xar de te~a -- mar
  a -- cin -- zen -- ta -- ram -- minh' -- al -- ma
  mas não ce -- ga -- ram o~o -- lhar

  a -- fi -- ve -- la -- ram meu pei -- to
  pra eu dei -- xar de te~a -- mar
  a -- cin -- zen -- ta -- ram -- minh' -- al -- ma
  mas não ce -- ga -- ram o~o -- lhar

  sau -- da -- de~a -- mor
  que sau -- da de
  que me vi -- ra pe -- lo~a -- ves -- so
  que re -- vi -- ra meu a -- ves -- so

  pu -- se -- ram~a fa -- ca em meu pei -- to
  mas quem dis -- se que~eu te~es -- que -- ço
  mas quem dis -- se que~eu me -- re -- ço
}

acordes = \chordmode {
  \set chordChanges = ##t
  \set majorSevenSymbol = \markup { maj7 }

  \partial 16 s16 |
  \repeat volta 2 {
    a2 |
    fis:m |
    b:7 |
    b:7 | \break
    b:m7.5- |
    e:7 |
    \alternative {
      { cis4:m c:m | b:m e:7 | }
      { a2 | e:7 | }
    } \break

    a2 |
    a |
    b:7 |
    b:7 | \break
    b:m7 |
    e:7 |
    a |
    e:7 | \break
    a |
    fis:7 |
    b:7 |
    b:7 | \break
    b:m7 |
    e:7 |
    dis:dim7 |
    a | \break
    a |
    fis:7 |
    b:7 |
    b:7 | \break
    b:m7 |
    e:7 |
    dis:dim7 |
    a | \break

    a:maj7 |
    fis:m7 |
    b:7 |
    b:7 | \break
    b:m7 |
    e:7 |
    cis4:m c:m |
    b:m e:7 | \break
    a2:maj7 |
    fis:m7 |
    b:7 | \break
    b:7 |
    b:m7 |
    e:7 |
    a |
    e:sus7 |
  }
}


\score {
  <<
    \new ChordNames \acordes
    \new Staff {
      \staffglobal
      \new Voice = "melodia" \melodia
    }

    \new Lyrics \lyricsto "melodia" \letra
  >>
  \midi {}
  \layout {}
}
