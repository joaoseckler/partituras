\version "2.24.4"
\include "sinc.ly"

\header {
  title = "O bom filho à casa torna"
  composer = "Bonfiglio de Oliveira"
  arranger = \markup \italic "Versão didática para violão de 7 por Gian Correa"
  tagline = \markup \teeny {
    "Editado com" \italic "lilypond" \normal-text
    "por João Seckler em 2024"
  }
}

\paper {
  system-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 3)
       (stretchability . 12))
  markup-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 3)
       (stretchability . 12))
}

staffglobal = {
  \key d \minor
  \time 2/4
  \set Timing.beatStructure = 1,1
}

melodia = \new Voice \relative c'' {
  \section \sectionLabel \markup \box A
  R2 * 4 |
  \repeat volta 2 {
    \sinc a a f d16 f a g~ |
    \sinc g g e a4 |
    r16 a d, e f16 g gis a |
    \sinc g g e a4 | \break
    \sinc r d, e f16 g a bes |
    \sinc c c b bes8. a16 |
    gis16 gis e e'~ e4 |
    r16 cis a g bes g e cis | \break

    \sinc a' a f d16 f a g~ |
    \sinc g g e a4 |
    \sinc r f' e d16 c b bes |
    \sinc a a e a4 | \break
    g4 fis |
    \sinc a g a bes16 c d8 |
    r16 f, f f f8 e |
    \alternative {
      { d4 r }
      { d4 r }
    }
  }
  \bar "||" \break

  \section \sectionLabel \markup \box B
  c16 d e f g a bes b |
  \sinc c c a d16 cis d bes~ |
  \sinc bes bes g c16 b c a~ |
  \sinc a a f  \tuplet 3/2 { bes8 a bes } | \break
  \tuplet 3/2 { g g e } \tuplet 3/2 { a gis a } |
  f4 r16 g f e |
  d e f e d e f g |
  a2 | \break
  c,16 d e f g a bes b |
  \sinc c c a d16 cis d bes~ |
  \sinc bes bes g c16 b c a~ |
  \sinc a a f  \tuplet 3/2 { bes8 a bes } | \break
  \tuplet 3/2 { g g e } \tuplet 3/2 { a gis a } |
  \sinc f a d \sinc bes g bes |
  \sinc a f a g16 f e g |
  f4 r4
  \bar "||" \pageBreak

  \section \sectionLabel \markup \box C
  \key d \major

  R2 * 4 | \break

  a2 |
  \sinc r fis a cis8 b16 a~ |
  a2 |
  \sinc r g a e'8 cis16 a~ | \break
  \sinc a fis g a8 b16 g~ |
  \sinc g e fis g8 a16 fis~ |
  fis2 |
  r8 fis g a | \break
  b2~ |
  \sinc b cis d cis8 b16 ais~ |
  ais4. cis8 |
  fis,4 r | \break
  g2~ |
  \sinc g a b a8 g16 fis~ |
  fis4 cis' |
  fis,2 | \break
  e2 |
  \sinc r fis g \sinc a b cis |
  d2 |
  \sinc r fis, g \sinc a b d | \break
  cis2 |
  \sinc r e, fis \sinc g a cis |
  b2( |
  a4) r | \break
  a8. g16~ \sinc g a g |
  d'2 |
  g,8. fis16~ \sinc fis g fis |
  b2 | \break
  fis8. e16~ \sinc e fis e |
  b'8. a16~ \sinc a e fis |
  d2 |
  R2 |
  \bar "||"
}

% maxixe com cabeça
maxixe_c =
#(define-music-function
   (n1 n2 n3)
   (ly:music? ly:music? ly:music?)
   #{
   \fixed c' {
     < $(set-duration n1 #{ 16 #}) $n2 $n3 >
     \deadNote $n1 < $n2 $n3 > < $n1 $n2 $n3 >16
     r16 $n1 < $n2 $n3 > \deadNote $n1
   }
   #}
 )

maxixeA =
#(define-music-function
   (n1 n2 n3)
   (ly:music? ly:music? ly:music?)
   #{
   \fixed c' {
     r16 \deadNote $n1 < $n2 $n3 > < $n1 $n2 $n3 >16
   }
   #}
)

maxixeB =
#(define-music-function
   (n1 n2 n3)
   (ly:music? ly:music? ly:music?)
   #{
   \fixed c' {
     r16 $n1 < $n2 $n3 > \deadNote $n1
   }
   #}
)

maxixeB_p =
#(define-music-function
   (n1 n2 n3)
   (ly:music? ly:music? ly:music?)
   #{
   \fixed c' {
     r16 $n1 < $n2 $n3 > \deadNote r16
   }
   #}
)


maxixe =
#(define-music-function
   (n1 n2 n3)
   (ly:music? ly:music? ly:music?)
   #{
   \fixed c' {
     \maxixeA $n1 $n2 $n3
     \maxixeB $n1 $n2 $n3
   }
   #}
)

% maxixe sem pé
maxixe_p =
#(define-music-function
   (n1 n2 n3)
   (ly:music? ly:music? ly:music?)
   #{
   \fixed c' {
     \maxixeA $n1 $n2 $n3
     \maxixeB_p $n1 $n2 $n3
   }
   #}
)

violao =
<<
  \new Voice {
    \voiceOne
    \relative c' {
      \repeat volta 2 {
        \maxixe_c a,16 d f |
        \maxixe_c g, cis e |
      }
      \repeat volta 2 {
        \maxixe f,16 a, d |
        \maxixe e,16 g, cis |
      }
      \repeat volta 2 {
        \repeat unfold 2 {
          \maxixe f,16 a, d |
          \maxixe e,16 g, cis |
        }
        r16 a <d f> a <f a d> f <a d> r16 |
        r16 e <a c> r s4 |
        r16 <e b' d>8 <e b' d>16 r <e, b' d gis b> d' e, |
        <a e' g cis> r8. r4 |

        \maxixe f,16 a d |
        \maxixeA e16 g cis s4 |

        \maxixe a,16 d f |
        \maxixeA g,16 cis e s4 |

        r16 g' <c es> g r fis <c' d> r16 |
        s4 r16 g <bes d> g |
        r8 <a d> <gis d'> <g cis> |
        \alternative {
          { <f a d>16 f <a d> f s4 | }
          { \maxixeA f,16 a, d s4 | }
        }
      }

      \maxixe_p e,16 bes, c |
      \maxixe_p a, d f |
      \maxixe_p e,16 bes, c |
      \maxixe_p a, d f |
      \maxixeA bes, d f \maxixeB g, cis e |
      s2 |
      r16 aes <d f> aes r16 f <bes d> f |
      \maxixeA e, g, cis r4 |

      r16 e <bes' c> r r16 e, <bes' c> r |
      r16 a <d f> r r16 a <d f> r |
      \maxixeA d,16 bes, d \maxixeB_p e, bes, c |
      \maxixeA a,16 d f r4 |
      \maxixeA bes,16 d g \maxixeB_p g, cis e |
      r4 r8 <f, g d'> |
      r16 f <a c> r r e <bes' c> r |
      s2 |

      \key d \major
      r16 fis <a b> r r fis <a b> r |
      r16 e <g cis> r r e <g cis> r |

      r fis <a b> r r fis <a b> r |
      r e g cis s4 |

      \repeat unfold 3 \relative c {
        r16 fis <a b> r r fis <a b> r |
        r16 e <g cis> r r e <g cis> r |
      }

      s2 |
      s2 |
      r16 d, g b r4 |
      r8 <g b>16 f r16 f <g b> r |
      r16 <e ais cis>8 e16 r e <ais cis> e |
      r8 <ais cis>16 e r16 e <ais cis> e |
      r16 <f b d>8 16 r16 f <b d> f |
      r8 <b d>16 f r16 f <b d> f |
      r16 <e ais cis>8 16 r4 |
      r8 <a b>16 dis, r8 <dis a' b> 16 16 |
      s2 |
      r8 <g b>16 e r16 e <g b> r |
      s2 |
      r8 <a d e> r16 a <d e> r |
      s2 |
      r8 <g, cis>16 e r4 |
      r16 <d aes' b>8 16 r16 d <aes' b> d, |
      <d a' b>8 r <fis c' d>4 |
      r8 d16 <g b> r16 g <g b> d |
      \maxixe e, bes, d |
      r8 <a' b>16 d, r4 |
      r4 r16 a' <b fis'> a |
      r8 <b e>16 g r4 |
      r8 <g b>16 e r16 e <g b> r |
      s2 |
    }
  }

  \new Voice {
    \voiceTwo
    \relative c {
      \repeat volta 2 {
        d16 r8. d4 |
        a16 r8. a4
      }
      \repeat volta 2 {
        d,16 r8. f4 |
        e16 r8. a4
      }
      \repeat volta 2 {
        \repeat unfold 2 {
          d,16 r8. f4 |
          e16 r8. a4
        }
        d4 b8. bes16 |
        a8. e'16 d c b a |
        gis4 s4 |
        s2 |

        d16 r8. f4 |
        e16 r8. a16 ais b cis |
        d4 f4 |
        e16 r8. a,16 g f e |
        es4 d8. fis16 |
        g fis r f e4 |
        d8 f e a |
        \alternative {
          { d,4 a'16 g f e | }
          { d4 r16 f e d | }
        }
      }

      c4 g'8. c,16 |
      f4 a16 bes a aes |
      g4 c,8. c16 |
      f4 f8. f16 |
      e4 a |
      d8 a'16 f e d c b |
      bes?4 aes8. aes16 |
      a?4 e16 f e d |

      c8. d16 e8. c16 |
      f8. g16 a8. aes16 |
      g4 c,8. c16 |
      f4 f16 a g f |
      e4 a8. a16 |
      \sinc d c ces bes4 |
      a8. aes16 g8. c,16 |
      f g a c f4 |
    }

    % C
    \relative c, {
      d8. a'16 fis8. a16 |
      e8. a16 g8. a16 |

      d,8. a'16 fis8. a16 |
      e8 r a16 g fis e |

      \repeat unfold 3 {
        d8. a'16 fis8. a16 |
        e8. a16 g8. a16 |
      }

      d8 gis,16 b a g fis e |
      d16 r d8( e fis |
      g4) b16 d g ges |
      f4 g,8. g16 |
      fis8 r fis4 |
      fis8 r fis4 |
      g8 r g4 |
      g8 r g4 |
      fis8 r fis16 gis a ais |
      b8 r fis4 |
      e16 e g g b b d d |
      cis4 a8. a16 |
      \sinc d a b \sinc cis d e |
      fis4 fis8. f16 |
      e8 g16 fis e d cis b |
      a4 \sinc r g fis |
      f4 f4 |
      fis8 r d4 |
      g8 r d4 |
      g8 r g4 |
      fis8 r fis16 g a ais |
      b bis cis cisis dis4 |
      e4 e,16 g b d |
      cis8 r a8. a16 |
      \sinc d b a \sinc g fis e |
      d8 r a'4 |
    }
  }
>>

acordes = \chordmode {
  \set chordChanges = ##t
  \set majorSevenSymbol = \markup { maj7 }

  \repeat unfold 2 {
    \repeat volta 2 {
      d2:m |
      a:7 |
    }
  }

  \repeat volta 2 {
    \repeat unfold 2 {
      d2:m |
      a:7 |
    }

    d4:m b:m7.5- |
    a2:m |
    e:7 |
    a:7 |

    \repeat unfold 2 {
      d2:m |
      a:7 |
    }

    a4:m7.5- d:7 |
    g4:m e:m7.5- |
    d4:m e8:7 a:7 |
    \alternative {
      { d2:m | }
      { d2:m | }
    }
  }

  c2:7 |
  f:6 |
  c:7 |
  f:6 |
  e4:m7.5- a:7 |
  d2:m
  bes:7 |
  a:7 |
  c:7 |
  f:6 |
  c:7 |
  f:6 |
  e4:m7.5- a:7 |
  d:m bes:6 |
  f c:7 |
  f2 |

  \repeat unfold 5 {
    d2:6 |
    a:7 |
  }

  d |
  d:7 |
  g |
  g:7 |
  fis:7 |
  fis:7 |
  g:7 |
  g:7 |
  fis:7 |
  b:7 |
  e:m |
  a:7 |
  d |
  d |
  a:7 |
  a:7 |
  d:dim |
  d4:6 d:7 |
  g2 |
  g:m6 |
  d:6 |
  b:7 |
  e:m |
  a:7 |
  d |
  d4 a:7 |
}

\score {
  <<
    \new Staff {
      \staffglobal
      \melodia
    }
    \new ChordNames \acordes
    \new Staff {
      \staffglobal
      \clef "treble_8"
      \violao
    }
  >>
  \layout {
    \context {
      \ChordNames
      \override VerticalAxisGroup.
      nonstaff-relatedstaff-spacing.padding = #2
      \override VerticalAxisGroup.
      nonstaff-unrelatedstaff-spacing.padding = #2
      \override LyricSpace.minimum-distance = #1.0
    }

  }
}
