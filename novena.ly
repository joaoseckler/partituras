\version "2.22.0"
\include "sinc.ly"

\header {
  title = "Novena"
  composer = "Geraldo azevedo"
  tagline = \markup \teeny {
    "Baseado na versão do cantoria. A forma na versão de estúdio original é um pouco diferente"
    "Editado com" \italic "lilypond" \normal-text
     "por João Seckler em nov/2022"
   }
}

staffglobal = {
  \key e \minor
  \time 2/4
  % \tempo 4 = 80
  \set Timing.beamExceptions = #'()
  \set Timing.beatStructure = 1,1,1,1
}

% acordes = \chordmode {
% }

riff_nas_horas = {
  <<
    \relative c' {
      \voiceOne
      e4 e~ |
      e g8 e |
    }
    \new Voice \relative c {
      \voiceTwo
      e2 |
      e2 |
    }
  >>
}

riff_principal = {
    <<
      \relative c' {
        \voiceOne

        r4 b''16( a) a8 |
        r4 a16( g) g8 |
        << g, dis' g >> << g, d' fis >> << g, cis e>> << g, b e >> |
        % g fis e e |
      }

      \new Voice \relative c'' {
        \voiceFour
        s4 << g8 fis' >> << g,8 fis' >> |
        s4 << g,8 e' >> << g, e' >> |
        s2
      }

      \new Voice \relative c {
        \voiceTwo
        e2 |
        e2~ |
        e2
      }
    >>
  }

melodia_intro = \relative c'' {
  \tag #'violao {
    b4_\markup {
      \tiny \italic
      \lower #3
      "Vai acompanhando com mi menor nas cordas 1, 2 e 3"
    } e,
  }
  \tag #'voz {
    b'8. e,16 e4 |
  }
  \sinc r fis g \sinc a g fis |
  \tag #'violao {
    b4 e, |
  }
  \tag #'voz {
    b'8. e,16 e4 |
  }
  \sinc r fis g \sinc a fis d |
  b8 e16 e~ e4 |
  \tuplet 3/2 { d8 c b } \tuplet 3/2 { d e g } |
  g4 r8. a16 |
  a8. a16 \sinc a b g |
  e4 r16 g c b |
  \sinc a g a b8 d |
  e2 |
  e8 d b a~ |
  \sinc a g a \sinc b a g |
  e8 d~ d b16 d |
  e8 g~ \sinc g a b |
  \sinc a g e \sinc d b a |
  \time 3/4
  b8 d4. g8
  e |
  \time 2/4
}

intro = \relative c'' {
  \keepWithTag #'violao \melodia_intro

  \repeat volta 2 \riff_principal
  \repeat unfold 2 \riff_nas_horas
}




comp = {
  \new OneStaff {
    \staffglobal
    \new Voice = "comp"
    \repeat unfold 6 \riff_nas_horas
    \repeat unfold 2 \riff_principal

    <<
      \relative c' {
        \voiceOne
        r4 b''16( a) a8 |
        r4 a16( g) g8 |
        r4 g16( fis) fis8 |
        r4 fis16( e) e8 |
      }

      \new Voice \relative c'' {
        \voiceFour
        s4 << g8 fis' >> << g,8 fis' >> |
        s4 << g,8 e' >> << g, e' >> |
        s4 << g,8 dis' >> << g, dis' >> |
        s4 << g,8 cis >> << g cis >> |
      }

      \new Voice \relative c {
        \voiceTwo
        e2 |
        e2 |
        e2 |
        e2 |
      }
    >>

    \new ChordNames {
      \chordmode {
        e2:m |
        a:m7 |
        d:7 |
        g |

      }
    }

    \relative c' {
      <<
        {
          \voiceOne
          r16 bes' d bes fis' bes, d bes |
          r16 e, a e c' e, a e |
          f' a, c fis a, c g' a, |
        }
        \new Voice \relative c' {
          \voiceTwo
          c2 |
          fis,2 |
          s2
        }
      >>
      \riff_principal
    }

    \repeat volta 2 {
      \relative c'' {
        <<
          {
            \voiceOne
            << b4_\markup { \small \italic \lower #5 "dedilhando"} e4 >>
            << d g >> |
            << es2 a >> |
            << e! bes' >> |
            << f bes >> |
            << fis!4 d' >>
            << fis, c' >> |
            << fis,!4 bes >>
            << f bes >> |
            << e, c' >>
            << e, bes' >> |
            << es, bes' >>
            << d, bes' >> |
            << cis, a' >>
            << cis, g'>> |
            << c,2 f >> |
            << c2 e >> |
            << c2 f >> |
            << c2 e >> |
            << c2 f >> |
          }
          \new Voice {
            \voiceTwo
            << e,4 g >>
            << d g >>
            \repeat unfold 13 { << d2 g >>}
          }
        >>
      }

      \new ChordNames {
        \chordmode {
          a4:m a:m/g |
          fis2:m7.5- |
          b:7 |
        }
      }

      \keepWithTag #'violao \melodia_intro
    }
    \alternative {
      {
        \repeat unfold 2 \riff_principal
      }
      {
        <<
          \relative c' {
            \voiceOne
            e4 e~ |
            e g8 e |
          }
          \new Voice \relative c {
            \voiceTwo
            e2 |
            e2_\markup {
              \italic "Do "
              \tiny \raise #1
              \musicglyph "scripts.segno"

              \italic \normalsize " ao "
              \raise #1
              \musicglyph "scripts.coda"
            }
          }
        >>
      }
    }
    \repeat volta 2 {
      <<
        \relative c' {
          \voiceOne
          r16 bes' d bes fis' bes, d bes |
          r16 e, a e c' e, a e |
          f' a, c fis a, c g' a, |
        }
        \new Voice \relative c' {
          \voiceTwo
          c2 |
          fis,2 |
          s2
        }
      >>
    }
    \alternative {
      {
        <<
          \relative c' {
            \voiceOne

            r4 b''16( a) a8 |
          }

          \new Voice \relative c'' {
            \voiceFour
            s4 << g8 fis' >> << g,8 fis' >> |
          }

          \new Voice \relative c {
            \voiceTwo
            e2 |
          }
        >>
      }
      {
        <<
          \relative c'' {
            \voiceOne
            r4 e'16 d d8 |
            r d c b |
            c b a b |
            a g a g |
            << g,8 dis' g >> << g, d' fis >> << g,4 cis e >> |
            \arpeggioArrowDown
            << e,,2 e' bes' b e_\markup { \lower #7 \tiny \italic "ô minino!"} \arpeggio>>
          }
          \new Voice \relative c {
            \voiceTwo
            e2 |
            e2 |
            e2 |
            e2_\markup {\italic \lower #8 rall.......................} |
            e2 |
            s2 |
          }
          \new Voice \relative c'' {
            \voiceFour
            s4 b'8 b |
            s a16 g, a' g, g' g, |
            g' g, g' g, fis' g, fis' g, |
            fis' g, e' g, e' g, e' g, |
            s2 | s2 |
          }
        >>
      }
    }
  }
}

voz = \relative c' {
  % nas horas de deus amei
  r4.^\markup {\larger \raise #3 \musicglyph "scripts.segno" }
  e16^\markup {\raise #3 "Voz"} g |
  \sinc b d cis b8 g~ |
  g4 \sinc r g b |
  \sinc d cis b cis16 a b8~ |
  b2

  % essa é a primeira cantiga
  \sinc e, g b \sinc d cis b |
  e4 \sinc d cis b |
  g8. b16 d16 cis b8~ |
  b2 |

  \sinc e, g b \sinc d cis b |
  e4 \sinc d cis b |
  g8. a16 fis16 e e8~ |
  e2 |

  \repeat unfold 4 { r2 |}

  % Sei que são nove dias
  r4. b'8~ |
  b4 a8 fis~ |
  fis16 e e e~ e4 |
  fis g8 fis~ |
  fis16 e8.~ e8. e16 |
  \sinc e e g a8 b~ |
  b16 a a8~ a16 a a c~ |
  c8 c16 b a8. g16~ |
  g4 r16 g16 g32 g g16 |
  a^\markup {\huge \raise #3 \musicglyph "scripts.coda" } fis8.~ fis8. fis16 |
  g8 e~ e4 |
  f8. fis16~ fis8 g~ |
  g16 e8. r4 |

  \repeat unfold 2 { r2 |}

  \repeat volta 2 {
    r16 g g8~ g16 a bes a~ |
    a4 g16 bes8.~ |
    bes8 g16 g g4~ |
    g4 r8 d'8~ |
    d4~ d16 c c8~ |
    c16 bes bes bes~ bes8. a16
    g c8.~ c16 bes bes bes~ |
    bes g8. \sinc g g a~ |
    a16 bes a8~ a16 g16 f8~ |
    f16 d d d~ d f a g~ |
    g4 \sinc r f g |
    \sinc a g f g16 a8.~ |
    a16 g f8 g16 a8 d16 |
    d2~ |
    d4 r8 c16 c16
    c2~ |
    c4 r8 b16 b  |

    \keepWithTag #'voz \melodia_intro

  }
  \alternative {
    {
      \repeat unfold 6 { r2 |}
    }
    {
      \repeat unfold 2 { r2 |}
    }
  }

  \break
  \repeat volta 2 {
    a16^\markup {\huge \raise #3 \musicglyph "scripts.coda" }
    fis8.~ fis8. fis16 |
    g8 e~ e4 |
    f8. fis16~ fis8 g~ |
  }
  \alternative {
    {
      g16 e8. r16 g16 g32 g g16 |
    }
    {
      g16\repeatTie e8. r4 |
    }
  }

  r2 |
  r4 a8 b |
  a g a g |
  g fis e e~ |
  e2
}

letra = \new Lyrics = "letra" \lyricsto "voz" {
  nas ho -- ras de deus a -- mém
  pa -- dre, fi -- lho~es -- pí -- ri -- to santo
  es -- sa~é~a pri -- mei -- ra can -- ti -- ga
  que nes -- sa ca -- sa eu canto

  es -- sa~é~a pri -- mei -- ra can -- ti -- ga
  que nes -- sa ca -- sa eu canto

  sei que são no -- ve dias no -- ve pe -- nas
  en -- quan -- to~a~es -- pe -- ra~au -- men -- ta~o
  mun -- do se faz es -- que -- ci -- do
  na ter -- ra dos ho -- mens
  de lu -- zes co -- lo -- ri -- das

  <<
    {
      en -- quan -- to~a fa -- mí -- lia re -- za no -- vena
      as no -- tí -- cias que mon -- tam ca -- va -- los li -- gei -- ros
      vão to -- man -- do to -- do~o mun -- do~e
      na ca -- sa do lar
      es -- que -- ci -- dos fi -- cam to -- dos lon -- ge de sa -- ber

      o que foi que a -- con -- te -- ceu
      e a -- li nin -- guém per -- ce -- beu
      tan -- ta pe -- dra de~a -- mor ca -- ir
      tan -- ta gen -- te se par -- tir
      no azul des -- sa~in -- crí -- vel dor
      en -- quan -- to~a fa -- mí -- lia re -- za~al -- guém
      se -- gue~a no -- ve -- na
      no abis -- mo de pre -- ces re -- pe -- ti -- das
      no sos -- se -- go de u -- ma~a -- go -- ni -- a sem fim
    }
    \new Lyrics \with { alignBelowContext = "letra" }
    {
      \set associatedVoice = "voz"
      en -- quan -- to~a fa -- mí -- lia re -- za no -- vena
      no -- ve di -- as se pas -- sam mar -- ca -- dos
      sem tem -- po sem na -- da e sem fim
      no meio do mun -- do, do medo
      e de mim des --  -- pe -- da -- ça -- do~em
      tan -- to ver -- so~en -- tão
      de~o -- ra -- ções a sa -- la se faz
      e lá fo -- ra se~es -- que -- ce~a paz
      u -- ma bom -- ba~ex -- plo -- diu por lá
      so -- bre~os o -- lhos do meu bem
      e~as -- sim me ma -- ta tam -- bém
      en -- quan -- to~a no -- ve -- na che -- ga~ao fim
      ban -- das ban -- dei -- ras
      ben -- di -- tos pas -- san -- do pe -- la vi -- da
      e~a no -- ve -- na se per -- de~es -- que -- ci -- da de nós
    }
  >>

  % CODA

  ho -- mens de lu -- zes co -- lo -- ri -- das
  na ter -- ra dos
  _ das hm __ _ _ _ _ _ _ _ _ _ _

}

\paper {
  system-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 3)
       (stretchability . 12))
  markup-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 3)
       (stretchability . 12))
}

\layout {
  \context {
    \Lyrics
      \override VerticalAxisGroup.
        nonstaff-relatedstaff-spacing.padding = #2
      \override VerticalAxisGroup.
        nonstaff-unrelatedstaff-spacing.padding = #2
      \override LyricSpace.minimum-distance = #1.5
  }
  \context {
    \ChordNames
      \override VerticalAxisGroup.
        nonstaff-relatedstaff-spacing.padding = #3
      \override VerticalAxisGroup.
        nonstaff-unrelatedstaff-spacing.padding = #3
    \override LyricSpace.minimum-distance = #1.5
  }
}

\score {
  {
    <<
      \new Staff {
        \staffglobal
        \new Voice = "intro" \intro
        \break
        <<
          \once \omit Staff.TimeSignature
          \once \set Staff.explicitKeySignatureVisibility = #end-of-line-invisible
          \new Voice = "voz" \voz
          \letra
          \comp
        >>
      }
    >>
  }
  \layout {
    \context {
      \Staff {
        \override VerticalAxisGroup.default-staff-staff-spacing =
        #'((basic-distance . 12)
        (minimum-distance . 9)
        (padding . 5)
        (stretchability . 12))
      }
    }
    \context {
      \ChordNames {
        \override ChordName #'font-size = #4
        \override ChordName #' Y-offset = #-1.5
      }
    }
  }
}

\paper {
  system-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 4)
       (stretchability . 12))
  markup-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 3)
       (stretchability . 12))
}


