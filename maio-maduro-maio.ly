\version "2.20.0"
\include "defaults.ly"

\header {
  title = "Maio, maduro maio"
  composer = "José Afonso"
  arranger = "Arr. José Mario Branco"
  tagline = \markup {
    \smaller \smaller
    \center-column {
      \line {"This is an adaptation of the original arrangement score"}
      \line {"https://arquivojosemariobranco.fcsh.unl.pt/sites/arquivojosemariobranco.fcsh.unl.pt/files/017_maio_maduro_maio_geral.pdf"}
    }
  }
}
overwriteCN = #(define-music-function (parser location text) (string?)
  #{\once \set ChordNames.chordChanges = ##f
    \once \override ChordNames.ChordName #'stencil = #(lambda (grob)
      (ly:grob-set-property! grob 'text (markup $text))
      (ly:text-interface::print grob)
     )
  #})

smallfont = #-2

staffglobal = {
  \key d \major
  %{ \partial 8 %}
  % \set Timing.beamExceptions = #'()
  % \set Timing.beatStructure = 1,1,1,1
}


%%%%%%%%%%%%% Melodic variables %%%%%%%%%%%%%5

mmm = \relative c'' { d8 d16 a d a d8 d16 a d a | b2. }
avmn = \relative c''' { a8 b cis b a8. fis16 | e2. }
trtrtr = \relative c'' {
  \ottava #1
  e8 fis16 e d b e8. d16 b8 | e fis16 e d b a4.
  \ottava #0
}
falua = \relative c''' { d16 cis b a fis e~ e d b a b8 | d2. }

baixo = \relative c' {
  <<
    { r2. | r2. | r2. }
    \new Staff \with {
      \remove "Time_signature_engraver"
      \magnifyStaff #2/3
      \override VerticalAxisGroup.default-staff-staff-spacing =
        #'((basic-distance . 0)
        (padding . 1))
      alignAboveContext = "mainstaff"
    }
    \relative c, {
      \staffglobal
      \omit Staff.KeySignature
      \clef F
      d16 e fis g a b cis d e fis g a |
      c,,8 a''16[ a8] g16[ g8] e16[ e8.] |
      d,2.
    }
  >>
}

baixosol = {
  <<
    { r2. | r2. }
    \new Staff \with {
      \remove "Time_signature_engraver"
      \magnifyStaff #2/3
      \override VerticalAxisGroup.default-staff-staff-spacing =
        #'((basic-distance . 0)
       (padding . -1))
    }
    \relative c {
      \staffglobal
      \omit Staff.KeySignature
      \clef F
      g16 a b c d e fis g a b c d |
      g,,2. |
    }
  >>
}

quatroporoito = \relative c'' {
  \time 4/8
  <<
    { \set fontSize = \smallfont es8[ c] } \\
    { \set fontSize = \smallfont r16 d8[ bes16] }
  >>
  \set fontSize = \smallfont
  g8[ es] |

  \time 6/8
  fis4. r4. |

}

cincoporoito = \relative c'' {
  \time 5/8
  \set fontSize = \smallfont
  es16 d c bes g8 es r |
    \time 6/8
  \unset fontSize
}

interum = \relative c'' {
  \quatroporoito
  <<
    { \set fontSize = \smallfont g'8 e b16 a~ a8 e e } \\
    { \set fontSize = \smallfont r16 fis'8 d a16~ a g8 d16 e8 }
  >>

  g2.~ |
  g |
}

%%%%%%%% HARMONIC VARIABLES %%%%%%%%%%

tudo = \chordmode {
  s2. |
  \parenthesize d |
  c:maj7 |
  d |
  d |
  c:maj7 |
  d |
  c:maj7 |
  d:maj7 |
  c:9
  b4.:m e |
  g d8 s4 |
  \set chordChanges = ##f
  <<
    \new ChordNames \with { alignBelowContext = "chords" } {
      g4 b8:m e4 a8:7 |
    }
    { d4. c:maj7 | }
  >>
  \set chordChanges = ##t
  d2. |
  c:maj7 |
  \time 4/8 aes4 c8:m f | \time 6/8
  d2.
  c4 e8:m a4 d8:7 |
  g2. |
  g2.

  \time 4/8 aes4 c8:m f | \time 6/8
  d2. |

  \time 5/8 aes4 c8:m f s8 | \time 6/8
  g4 b8:m e4 a8:7 |
  c4 e8:m a4 d8:7 |
  g2. |
  g2.
}


%%%%%%%% LYRICS VARIABLES %%%%%%%%%%

la = \lyricmode {
  Ma -- io ma -- du -- ro Ma -- io, quem te pin -- tou
  quem te que -- brou o_en -- can -- to, nun -- ca te_a -- mou
  rai -- av -- a_o sol já no Sul
  ti ri tu ri tu ri tu ru
  ti ri tu ru tu ru
  e_u -- ma fa -- lu -- a vi -- nha lá de_Is -- tam -- bul
}

lb = \lyricmode {
  sem -- pre de -- pois da ses -- ta cha -- man -- do_as flores
  e -- ra_o di -- a da fes -- ta Ma -- io de_a -- mores
  e -- ra_o di -- a de can -- tar
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _
  e_u -- ma fa -- lu -- a_an -- da -- va_ao lon -- ge_a va -- rar
}

lc = \lyricmode {
  ma -- io com meu a -- mi -- go quem de -- ra já
  sem -- pre no mês do tri -- go se can -- ta -- rá
  qu'im -- por -- ta_a fú -- ria do mar
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _
  que_a voz não te_es -- mo -- re -- ça va -- mos lu -- tar
}

ld = \lyricmode {
  nu -- ma ru -- a com -- pri -- da El\-rei pas -- tor
  ven -- de_o so -- ro da vi -- da que ma -- ta_a dor
  an -- da ver ma -- io nas -- ceu
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _
  que_a voz não te_es -- mo -- re -- ça_a tur -- ba rom -- peu
}

fim = \lyricmode {
  que_a voz não te_es -- mo -- re -- ça_a tur -- ba rom -- peu
}

%%%%%%%%%% SCORE %%%%%%%%%%%%%%%%

\score {
  <<
    \new ChordNames = "chords" \chordmode {
      \set majorSevenSymbol = \markup { maj7 }
      \set chordChanges = ##t
      \tudo
    }

    \new Staff = "mainstaff" {
      \time 6/8
      \staffglobal
      \repeat volta 4 {

        \new Voice {
          r2.
          \baixo
        }

        \new Voice = "melodia" \relative c'' {
          \mmm
          \mmm
          \avmn
          \trtrtr
          \falua

        }
      }
      \alternative {
        { r2. }
        { \interum }
        { \quatroporoito }
        { \cincoporoito }
      }
      \new Voice = "fim" {
        \falua
      }
      \baixosol
    }

    \new Lyrics \lyricsto "melodia" {
      \la
    }
    \new Lyrics \lyricsto "melodia" {
      \lb
    }
    \new Lyrics \lyricsto "melodia" {
      \lc
    }
    \new Lyrics \lyricsto "melodia" {
      \ld
    }
    \new Lyrics \lyricsto "fim" {
      \fim
    }
  >>
}

