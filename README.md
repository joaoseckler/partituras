# Partituras

A set of sheet music made with lilypond.

## Dependencies

 - [Lilypond](https://lilypond.org)

## Compiling

 - `$ make`
