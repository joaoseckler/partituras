\version "2.22.0"

\header {
  title = \markup { "Boa noite, Amor" \normal-text \large "(1936)" }
  composer = "José Maria de Abreu e Fransisco Matoso"
  arranger = "arr. S. Kerr"
  tagline = \markup \tiny {  "editado com" \italic "lilypond" \normal-text "por João Seckler em set/2022"}
}

\layout {
  \context { \Staff \remove "Instrument_name_engraver" }
}

Key = \key f \major
Time = {
  \time 3/4
  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = 1,1,1
  \tempo 4 = 90
  \set Score.tempoHideNote = ##t
}

with-breaks = ##t

conditional-break =
#(define-music-function () ()
   (if with-breaks
     #{
       \break
     #}
     #{
       {}
     #}
     )
   )

% https://lsr.di.unimi.it/LSR/Snippet?id=715
extendLV =
#(define-music-function (further) (number?)
#{
  \once \override LaissezVibrerTie.X-extent = #'(0 . 0)
  \once \override LaissezVibrerTie.details.note-head-gap = #(/ further -2)
  \once \override LaissezVibrerTie.extra-offset = #(cons (/ further 2) 0)
#})

extendRT =
#(define-music-function (parser location further) (number?)
   #{
     \once \override RepeatTie.X-extent = #'(0 . 0)
     \once \override RepeatTie.details.note-head-gap = #(/ further -2)
     \once \override RepeatTie.extra-offset = #(cons (/ further -2) 0)
   #})

SopranoInstrumentName = "Soprano"
AltoInstrumentName = "Contralto"
TenorInstrumentName = "Tenor"
BassInstrumentName = "Baixo"

SopranoMidiInstrument = "choir aahs"
AltoMidiInstrument = "choir aahs"
TenorMidiInstrument = "choir aahs"
BassMidiInstrument = "choir aahs"

SopranoMusic = \relative {
  d'4 e f |
  g a bes |
  c2 f,4 |
  e2 d4 |
  bes'2 e,4 |
  d2 c4 |
  \phrasingSlurUp
  a'2.~\( a | \conditional-break
  g~ | g\) |
  \shape #'((0 . 0) (0 . 1) (0 . 1) (0 . 0)) PhrasingSlur
  a~\( | a |
  g  | f |
  <<
    \once \override TextScript.avoid-slur = #'inside
    \once \override TextScript.outside-staff-priority = ##f
    {\voiceOne a2.^\markup {\translate #'(0 . 1) div} | g2\fermata\) }
    \new Voice { \voiceTwo f2. | e2 }
  >> \oneVoice c4 \conditional-break

  cis2 d4 |
  f2 d4 |
  dis2 e4 |
  a2 e4 |
  g2 f4 |
  c'2 e,4 |
  d2. |

  r2 d4 |
  dis2 e4 |\conditional-break
  a2 e4 |
  g2 f4 |
  e'2( d4) |

  a8 8 8 8 8 8 |
  d2. |
  aes8 8 8 8 8 8 |
  c2 c,4 | \conditional-break

  cis2 d4 |
  f2 d4 |
  dis2 e4 |
  a2 e4 |
  g2 f4 |
  c'2 cis4 |
  d2. \conditional-break

  \repeat volta 2 {
    <<
      { \voiceOne d2\rest }
      \new Voice {
        \voiceTwo \override ParenthesesItem.font-size = #-2
        \extendRT #2.0
        \parenthesize f,4\repeatTie s4
      }
    >>
    \oneVoice
    cis'8 d |
    f2 d8 bes |
    a4 a g8 a |
    c2 a8 f |
  }

  \alternative {
    {
      e4 e c8 c |
      cis4 cis d |
      a'2 a4 |
      \extendLV #2.0
      f2.\laissezVibrer |
      % \laissezVibrer is basically a half-tie to the right, like \repeatTie is a half-tie to the left.
    }
    {
      e4 e2 |
      f4. g8 a bes |
      a4. g8 a4 |
      a4. g8 a bes |
      <<
        {
          \voiceOne
          c4 d f |
          a2. |
        }
        \new Voice {
          \voiceTwo
          a,4 bes d |
          e d2 |
        }
      >> \oneVoice
    }
  }
}

SopranoLyrics = \lyricmode {
  quan -- do a noi -- te des -- cer
  in -- si -- nu -- an -- do~um tris -- te~a -- deus __  _
  ah! __ _ _ _ _

  boa noi -- te~a -- mor
  meu gran -- de~a -- mor
  con -- ti -- go~eu so -- nha -- rei

  e~a mi -- nha dor es -- que -- ce -- rei __

  <<
    { ao pen -- sar que~os so -- nhos meus se -- rão mes -- mo so -- nhos teus }
    \new Lyrics  \with { alignBelowContext = "SopranoLyrics" }
    { se~eu sou -- ber que~o so -- nho teu é o mes -- mo so -- nho meu}
  >>

  boa noi -- te~a -- mor
  e so -- nhe~en -- fim
  pen -- san -- do sem -- pre~em mim

  na ca -- rí -- cia de~um bei -- jo
  que fi -- cou no de -- se -- jo
  bo -- a noi -- te, meu gran -- de~a -- mor

  se -- jo
  Ah __ _
  meu _ gran -- de a -- mor __ \repeat unfold 8 { _ }
}

AltoMusic = \relative {
  d'4 e f |
  g a bes |
  c2 f,4 |
  e2 d4 |
  d2 e4 |
  d2 c4 |
  \phrasingSlurUp
  e2.\( |
  d |
  f |
  e\) |
  g4\( f e |
  dis2. |
  e |
  d! |
  d2 des4 |
  c2\)

  c4 |
  cis2 d4 |
  d2 d4 |
  dis2 e4 |
  e2 e4 |
  g2 f4 |
  c2 des4 |
  c2. |
  r2

  d4 |
  dis2 e4 |
  a2 e4 |
  g2 f4 |
  f2.~ |
  f~ |
  f~ |
  f~ |
  f4( e)

  c |
  cis2 d4 |
  d2 d4 |
  dis2 e4 |
  e2 e4 |
  e2 f4 |
  es2 es4 |
  d2. |

  \repeat volta 2 {

    \extendRT #2.0
    \override ParenthesesItem.font-size = #-2
    \parenthesize
    c,'4 \repeatTie r2.*2/3 |
    g'2. |
    e4. d8 e4 |
    f4. e8 d4 |
  }
  \alternative {
    {
      c2 8 8 |
      bes4 4 4 |
      4( f') e |
      \extendLV #2.0
      c2.\laissezVibrer
    }
    {
      c2. |
      bes |
      f'2. |
      f4. g8 a bes |
      a4 g bes |
      a2. |
    }
  }
}

AltoLyrics = \lyricmode {
  quan -- do a noi -- te des -- cer
  in -- si -- nu -- an -- do~um tris -- te~a -- deus __ _ _ _
  ah! _ _
  _ _ _ _ _ _

  boa noi -- te~a -- mor
  meu gran -- de~a -- mor
  con -- ti -- go~eu so -- nha -- rei

  e~a mi -- nha dor es -- que -- ce -- rei __

  boa noi -- te~a -- mor
  e so -- nhe~en -- fim
  pen -- san -- do sem -- pre~em mim _

  Ah! __ _ _ _ _ _ _ _
  bo -- a noi -- te, meu gran -- -- de~a -- mor

  \markup \with-color "white" "A" __ \repeat unfold 13 { _ }
}
TenorMusic = \relative {
  d4 e f |
  g a bes |
  a2.\( |
  b |
  bes!~ |
  2\) 4 |
  c2.~\( |
  2. |
  bes~ |
  4\) a bes |
  c2 c4 |
  b2 a4 |
  g c e, |
  f a d |
  bes!2.~ |
  2

  c,4 |
  cis2 d4 |
  bes'2 d4 |
  cis2 c4 |
  bes2 e,4 |
  g2 f4 |
  c'2 b4 |
  bes!2. |

  r2 d,4 |
  dis2 e4 |
  a2 e4 |
  g2 f4 |
  c'2.~\( |
  c |
  b |
  bes!~ |
  bes2\)

  c4 |
  cis2 d4 |
  bes2 d4 |
  cis2 c4 |
  bes2 bes4 |
  a2 a4 |
  g2 ges4 |
  f2. |

  \repeat volta 2 {
    <<
      { \voiceOne d'2\rest }
      \new Voice {
        \voiceTwo \override ParenthesesItem.font-size = #-2
        \extendRT #2.0
        \parenthesize a4\repeatTie s4
      }
    >>
    \oneVoice
    cis8 d |
    d4. cis8 d4 |
    bes4 a bes8 bes |
    a4 g f |
  }

  \alternative {
    {
      fis fis fis8 fis |
      f!4 f e |
      d( bes') bes |
      \extendLV #2.0
      a2.\laissezVibrer
    }
    {
      fis4 fis c'8 c |
      cis4 4 d |
      bes bes c |
      <<
        {\voiceOne f2.~ | f~ | f}
        \new Voice { \voiceTwo c2.~ | c~ | c}
      >> \oneVoice
    }
  }
}
TenorLyrics = \lyricmode {
  quan -- do a noi -- te des -- cer __ _ _
  a deus __ _
  o -- lhos -- teus
  hei de bei -- jan -- do teus de -- dos di -- zer __

  boa noi -- te~a -- mor
  meu gran -- de~a -- mor
  con -- ti -- go~eu so -- nha -- rei

  e~a mi -- nha dor es -- que -- ce -- rei __ _ _

  boa noi -- te~a -- mor
  e so -- nhe~en -- fim
  pen -- san -- do sem -- pre~em mim

  na ca -- rí -- cia de~um bei -- jo
  que fi -- cou no de -- se -- jo
  bo -- a noi -- te, meu gran -- de~a -- mor

  % volta 2
  se -- jo
  Bo -- a noi -- te meu gran -- de a -- mor __

}
BassMusic = \relative {
  d4 e f |
  g a bes |
  a2.\( |
  aes |
  g~ |
  2\) 4 |
  f2.~ |
  2. |
  d4 e f |
  g a bes |
  c2 c4 |
  b2 a4 |
  g c e, |
  f a b, |
  g'2.~|
  2_\fermata

  c,4 |
  cis2 d4 |
  g2.~ |
  g2.~ |
  g2 e4 |
  g2 f4 |
  a2 aes4 |
  g2. |

  r2 d4 |
  dis2 e4 |
  a2 e4 |
  g2 f4 |
  d2.~\( |
  d~ |
  d |
  des |
  c2\)

  c'4 |
  bes2 a4 |
  g2 f4 |
  e2 d4 |
  cis2 cis4 |
  c!2 c4 |
  a2 a4 |
  bes2. |

  \repeat volta 2 {
    % <<
    %   { \voiceOne r2. }
    %   \new Voice { \voiceTwo \override ParenthesesItem.font-size = #-2 \parenthesize f'4 s2}
    % >>
    % \oneVoice

    \override ParenthesesItem.font-size = #-2
    \extendRT #2.0
    \parenthesize f'4\repeatTie r2.*2/3 |
    bes,2. |
    c2.~ |
    c |
  }

  \alternative {
    {
      a4( bes) a |
      g g g |
      d'( cis) c |
      \extendLV #2.0
      f2.\laissezVibrer
    }
    {
      a,4( bes) a |
      g2.~ |
      g2 r4 |
      f'2.~ |
      f~ |
      f \bar "|."
    }
  }
}

BassLyrics = \lyricmode {
  quan -- do a noi -- te des -- cer __ _ _
  a deus __
  o -- lhan -- do nos o -- lhos -- teus
  hei de bei -- jan -- do teus de -- dos di -- zer __

  boa noi -- te~a -- mor __
  % meu gran -- de~a -- mor
  con -- ti -- go~eu so -- nha -- rei

  e~a mi -- nha dor es -- que -- ce -- rei __ _ _

  boa noi -- te~a -- mor
  e so -- nhe~en -- fim
  pen -- san -- do sem -- pre~em mim _

  Ah! __ _
  bo -- a noi -- te, meu gran -- de~a -- mor

  % volta 2
  Bo -- a noi __ ah __
}

% PianoRHMusic = {
%   \partCombine
%   << \SopranoMusic>>
%   << \AltoMusic>>
% }

% PianoLHMusic = {
%   \partCombine
%   << \TenorMusic>>
%   << \BassMusic>>
% }

\include "satb.ly"

