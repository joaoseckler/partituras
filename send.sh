hostname=jseckler.xyz
folder=partituras
website=jseckler.xyz
venv="set -a && . .env && set +a && . venv/bin/activate "

cmd="cd $folder && git pull --rebase && make"
cmd+=" && cd ../$website && $venv && ./manage.py collectstatic --noinput"

echo "$cmd"

ssh "$hostname" "$cmd"
