\version "2.24.4"
\include "defaults.ly"
\include "sinc.ly"

\header {
  title = "Onde a dor não tem razão"
  composer = "Paulinho da Viola"
}

staffglobal = {
  \key e \major
  \time 2/4
  \set Timing.beamExceptions = #'()
  \set Timing.beatStructure = 1,1,1,1

}

intro = \relative c'' {
  \partial 16 gis'16~ | gis4.. fis16~ | fis2 | r4 \sinc r gis fis |
  \sinc e dis cis \sinc dis fis dis | cis4.. b16~ | b2 |
  r4 \sinc r e dis | \sinc cis b bes \sinc a gis cis | gis8. fis16~ fis4 |
  r8 cis'16 dis~ \sinc dis e a~ | a2 | gis4.. b16~ | b2 | r2
}

melodia = \relative c' \repeat volta 2 {
  \section \sectionLabel \markup \box A
  e2 |
  e4 r |
  r \sinc r cis  dis |
  e8 cis16 e~ \sinc e cis' cis~ |
  cis2 |
  c4.. b16~ |
  b4 r4 |
  r2 |
  \sinc r cis b \sinc gis b gis |
  \sinc eis gis eis \sinc cis bis cis |
  a'4~ \sinc a gis fis |
  cis4 r4 |
  \sinc a' a a \sinc a b a |
  \sinc gis fis eis \sinc fis b gis |
  e8. e16 \sinc r fis gis |
  e8 fis16 gis~ \sinc gis e gis~ |
  gis8. gis16 \sinc r e dis |
  cis8 dis16 e~ \sinc e dis fis~ |
  fis2 |
  r2 |
  \sinc a b cis \sinc e cis a |
  gis4~ \sinc gis fis dis~ |
  dis4~ \sinc dis fis cis~ |
  cis4 r4 |
  \sinc a' b cis \sinc e cis a |
  gis4~ \sinc gis fis fis~ |
  fis4~ \sinc fis gis eis~ |
  eis4 r4 |
  gis2 |
  fis4 r4 |
  r4 \sinc r eis fis |
  \sinc b a gis \sinc fis gis a |
  cis2 |
  b4 r4 |
  r4 \sinc r cis b |
  \sinc a gis fis \sinc a gis fis |
  c4 r4 |
  \sinc r fis, a \sinc d fis a |
  gis2 |
  dis4.. fis16~ |
  fis8. e16~ e4 |
  r2 \bar "||" \break

  \section \sectionLabel \markup \box B

  a8 b16 cis e8 cis16 a |
  gis8 fis16 gis fis8 e16 dis |
  dis8. cis16~ cis4 |
  \sinc r cis dis \sinc e gis dis |
  cis4 r4 |
  a'4~ \sinc a dis, cis~ |
  \sinc cis dis cis~ cis4 |
  r2 |
  \sinc ais ais ais e'16 e e dis~ |
  dis cis dis cis \sinc b ais dis~ |
  dis dis8. r4 |
  \sinc r fis gis ais16
  \once \override Score.Footnote.annotation-line = ##f
  \footnote #'(0 . 5) \markup \justify {
    É o que se ouve nas duas repetições da gravação de 1981. Talvez seja
    um artefato do portamento da voz do Paulinho
  }
  fisis8 ais16 |
  gis8. fis16~ fis4 |
  \sinc r fis gis \sinc ais fis ais |
  b2 |
  r2
}


letra = \lyricmode {
  Can -- to
  Pra di -- zer que no meu co -- ra -- ção
  Já não mais se a -- gi -- tam as on -- das de u -- ma pai -- xão
  E -- le não é mais a -- bri -- go de~a -- mo -- res per -- di -- dos
  É um la -- go mais tran -- qui -- lo
  On -- de~a dor não tem ra -- zão
  Ne -- le~a se -- men -- te de~um no -- vo~a -- mor nas -- ceu
  Li -- vre de to -- do ran -- cor em flor se~a -- briu
  Ve -- nho re -- a -- brir as ja -- ne -- las da vi -- da
  E can -- tar co -- mo ja -- mais can -- tei
  Es -- ta fe -- li -- ci -- da -- de~a -- in -- da

  Quem es -- pe -- rou co -- mo eu por um no -- vo ca -- ri -- nho
  E vi -- veu tão so -- zinho
  Tem que~a -- gra -- de -- cer
  Quan -- do con -- se -- gue do pei -- to ti -- rar um es -- pi -- nho
  É que~a ve -- lha~es -- pe -- ran -- ça
  Já não po -- de mor -- rer
}

acordes = \chordmode {
  \set chordChanges = ##t
  \set majorSevenSymbol = \markup { maj7 }
  \partial 16 s16
  fis2:7 |
  fis:7 |
  b:7 |
  b:7 |
  e:6 |
  e:6 |
  cis:7 |
  cis:7 |
  fis:7 |
  fis:7 |
  fis:m |
  b:7 |
  e:6 |
  b:7 |


  \repeat unfold 4 { e2:6 | }
  fis:7 |
  f:7 |
  e:6 |
  b:7 |
  e |
  cis:7 |
  fis:m |
  cis:7 |
  fis:m |
  gis:7 |
  cis:m |
  cis:m |
  fis:7 |
  fis:7 |
  fis:m |
  b:7 |
  fis:m |
  b:7 |
  e:6/gis |
  g:dim7 |
  fis:m |
  b:7 |
  gis:m7.5- |
  cis:7 |
  fis:m |
  fis:m |
  a:m/c |
  b:7 |
  e:6 |
  e:6 |
  cis:7 |
  cis:7 |
  a:m/c |
  a:m/c |
  fis:7 |
  b:7 |
  e:6 |
  e:6 |

  fis:m |
  b:7 |
  e:6/gis |
  cis:7 |
  fis:m |
  b:7 |
  e:6 |
  e:6 |
  dis:7 |
  dis:7 |
  gis:m |
  gis:m |
  fis:7 |
  fis:7 |
  fis:m |
  b:7
}


\score {
  <<
    \new ChordNames \acordes
    \new Staff {
      \staffglobal
      \repeat segno 2 {
        \new Voice = "intro" \intro \fine \break
        \new Voice = "melodia" \melodia
      }
    }

    \new Lyrics \lyricsto "melodia" \letra
  >>
  \midi {}
  \layout {}
}
