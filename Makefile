EXCLUDE=defaults.ly sinc.ly empty-staff.ly add_mark.ly
LY_FILES:=$(wildcard *.ly)
LY_FILES:=$(filter-out $(EXCLUDE), $(LY_FILES))
PDF_FILES=$(LY_FILES:.ly=.pdf)

.PHONY: all clean

all: $(PDF_FILES)

%.pdf: %.ly
	lilypond -dno-point-and-click "$<" 2>&1 | ./lilypond-color.sh
	@{ command -v recently_used && recently_used "$@"; } > /dev/null || true

clean:
	rm -f *.pdf *.midi
