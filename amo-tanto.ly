\version "2.20.0"
\include "defaults.ly"

\header {
  title = "Amo tanto"
  composer = "Jards Macalé"
}

staffglobal = {
  \key g \minor
  \time 4/4
  %{ \partial 8 %}
  \set Timing.beamExceptions = #'()
  \set Timing.beatStructure = 1,1,1,1

}

\score {
  <<
    \new ChordNames \chordmode {
      c2:m d:7 |
      g:m es:7 |
      aes:7 d:7 |
      g:m d:7 |

      \repeat unfold 3 {
        g1:m |
        a2:m7.5- d:7 |
        d:m7.5-  g:7 |
        c:m c:m/bes |
        a:m7.5- d:7 |
        g:m g:m/f |
      }
      \alternative {
        {
          a1:7 |
          a2:m7.5- d:7 |
        }
        {
          a2:7 d:7 |
          g1:m |
        }
        {
          a1:7 |
          a2:m7.5- d2:7 |
          g1:m |
        }
      }
    }

    \new Staff {
      \staffglobal

      \new Voice = "intro"
      \relative c''' {
        d8 c c bes bes a gis a |
        c bes bes a a4 g,4\turn |
        bes8 aes aes g g fis eis fis |


        <<
          { g1 | }
          \new Staff \with {
            \remove "Time_signature_engraver"
            \magnifyStaff #2/3
            \override VerticalAxisGroup.default-staff-staff-spacing =
              #'((basic-distance . 0)
             (padding . -1))
          }
          \relative c {
            \clef F
            g8 a bes c cis d e fis |
          }
        >>
      }

      \new Voice = "melodia" \relative c'' {
        % A
        <<
          { d4. d,8 g2 | }
          \new Staff
            \with {
              \remove "Time_signature_engraver"
              \magnifyStaff #2/3
            }
            \relative c' { \clef F g1 | }
        >>
        r4 \tuplet 3/2 {a8 bes a} es' d4. |
        r4 c8 b aes'4. b,8 |
        d4. c8 c2 |
        r4 bes8 a8~ a4. d16 d |
        c4. bes8 a2 |
        g8. cis16 cis4 cis4 r8 g |
        fis4. c'8 c2 |

        % A'
        r4 d8 d, g bes~ bes4 |
        r4 bes8 a es' d~ d4 |
        r4 c8 b aes'4. b,8 |
        d4. c8 c2 |
        r8 bes4 a8 es' d4. |
        c8 bes~ bes2~ bes8 a16 g |
        bes4 c2 a8 bes |
        <<
          {
            g2 r2 |
            r4 d'8 d, g2  |
          }

          \new Staff
            \with {
              \remove "Time_signature_engraver"
              \magnifyStaff #2/3
            }
            \relative c {
              \clef F
              g8 a bes c cis d e fis |
              g1
            }
        >>

        % A''
        r4 bes8 bes16 a16 es'2 |
        r4 c8 b aes'4. b,8 |
        d4. c8 c2 |
        r4 bes8 a8 es' d~ d4 |
        r4. c16 bes a4. g8 |
        bes2~ bes4. c16 a~ |
        a2~ a4. bes8 |
        g2 r2 |
      }
    }

    \new Lyrics \lyricsto "melodia" {
      meu a -- mor
      eu vim de lon -- ge
      pra di -- zer\-te_o meu a -- mor
      tem -- po tan -- to_eu te per -- di
      vim di -- zer\-te o que so -- fri

      fiz\-me di -- a
      fiz\-me noi -- te
      fiz\-me só pen -- san -- do_em ti
      fiz da mi -- nha vi -- da
      u -- ma_e -- ter -- na so -- li -- dão

      meu a -- mor
      vim te di -- zer
      que sem ti não sei vi -- ver
      vem co -- mi -- go
      que sem teu a -- mor
      me -- lhor mor -- rer
    }
  >>
}

