\version "2.22.0"

\header {
  title = "Can she excuse My wrongs"
  composer = "John Downland"
  poet = \markup \smaller { "The First Booke of Songes or Ayres, n. 1" }
  tagline = \markup \tiny {  "editado com" \italic "lilypond" \normal-text "por João Seckler em jan/2023"}
}

#(set-global-staff-size 19)

% \layout {
%   \context { \Staff \remove "Instrument_name_engraver" }
% }

Key = \key c \major
Time = {
  \time 3/4
  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = 1,1,1
  \tempo 4 = 90
  \set Score.tempoHideNote = ##t
}

SopranoInstrumentName = "Cantvs"
AltoInstrumentName = "Altvs"
TenorInstrumentName = "Tenor"
BassInstrumentName = "Bassvs"

SopranoMidiInstrument = "choir aahs"
AltoMidiInstrument = "choir aahs"
TenorMidiInstrument = "choir aahs"
BassMidiInstrument = "choir aahs"

SopranoMusic = \relative c' {
  \repeat volta 2 {
    d4 a' d |
    c4. b8 a4 |
    a g f |
    e2. |
    c'4 a8 bes4 g8 |
    a2 f4 |
    g e e |
    d2. |
  }

  \repeat volta 2 {
    a'4 a a |
    a a8 a4 b8 |
    c2 bes4 |
    a2. |
    c4 a8 bes4 g8 |
    a2 bes4 |
    a d d |
    cis2. |
  }

  \repeat volta 2 {
    a8. a16 a8 a4 a8 |
    a4 a8 a4. |
    b8. b16 b8 b4 b8 |
    b4 b8 b4 b8 |
    a4 a8 a a a |
    a4 a a8 g |
    fis8. g16 a8 b g a |
    fis2 d4 |
  }
}

SopranoLyrics = \lyricmode {
  <<
    {
      Can she ex -- cuse my wrongs with vir -- tues cloak
      shall I call her good when she proves un -- kind
      No, no where sha -- dows do for bo -- dies stand
      thou mayst be a -- bused if thy sight be dim
    }
    \new Lyrics {
      Are those clear fires which van -- ish in to smoke
      must I praise the leaves where no fruit I find
      Cold love is like to words writ -- ten on sand
      or to bub -- bles which on the wa -- ter swim
    }
  >>

  Wilt thou be thus a -- bu -- sed still
  see -- ing that she wil right thee ne -- ver
  if thou canst not o'er -- come her will
  thy love will be thus fruit -- less e -- ver
}

AltoMusic = \relative c' {
  \repeat volta 2 {
    f4 f4. g8 |
    a4. g8 f4 |
    f e d |
    cis2. |
    e4 f8 g4 e8 |
    f4. e8 d4 |
    d2 cis4 |
    d2.
  }
  \repeat volta 2 {
    e4 cis d8 e |
    f4 e8 d4 d8 |
    e8. f16 g8 a g8. f16 |
    e2. |

    e4 f8 g4 e8 |
    f4. a8 g4~ |
    g8 f4 d8 f g |
    a2. |
  }
  \repeat volta 2 {
    a,8. b16 c8 d4 c8 |
    f4 e8 d4 c8 |
    b8. c16 d8 e4 d8 |
    g4 f8 e4 d8 |
    cis8. d16 e4 f8 g |
    e8. g16 f8 d cis8. cis16 |
    d4 a8 d4 cis8 |
    d2 d4
  }
}

AltoLyrics = \lyricmode {
  <<
    {
      Can she ex -- cuse my wrongs with vir -- tues cloak
      shall I call her good when she proves un -- kind

      No, no where _ sha -- dows do  where sha -- dows do for bo -- dies stand
      thou mayst be a -- bused  a -- bused if thy sight be dim
    }
    \new Lyrics {
      Are those clear fires which van -- ish in to smoke
      must I praise the leaves where no fruit I find

      Cold love is _ like to words writ like to words writ -- ten on sand
      or to bub -- bles which on the wa -- ter wa -- ter swim
    }
  >>

  Wilt thou be thus a -- bu -- sed still _
  see -- ing that she wil right thee ne -- ver
  if thou canst not o'er -- come her will
  thy love will be thus fruit -- less e -- ver


}

TenorMusic = \relative c' {
  \repeat volta 2 {
    a4 d8. e16 f4 f4. d8 c4 c4 c4 a4 a2. a4 a8 d4 c8 c2 bes4 bes4 a4. g8 fis2.
  }

  \repeat volta 2 {
    cis'8. d16 e4 d8 cis8 d4 a8 f4 f8 g8. f16 d8 f8 e8 d'8 cis2.
    a4 a8 d4 c8 c4. f8 e8 d8 cis4 d8 f4 e16 d16 e2.
  }

  \repeat volta 2 {
    r4. a,8~ a16 b16 c8 d4 c8 f4 e8 d4. b8~ b16 c16 d8 e4 d8 g4 f8 e8. d16
    cis8. cis16 d16 d16 e8 cis8 cis8 d8 f8 e4 r8 d8~ d16 c16 b8 e8 e8 a,4 a2
  }
}

TenorLyrics = \lyricmode {
  <<
    {
      Can she _ ex -- cuse my wrongs with vir -- tues cloak
      shall I call her good when she proves un -- kind

      No _ no no where sha -- dows do for bo__ __ _ dies for bo -- dies stand
      thou maist be a -- bused if thy _ sight thy sight be _ dim
    }

    \new Lyrics {
      Are those _ clear fires which va -- nish in to smoke
      must I praise the leaves where no fruit I find

      Cold _ love love is like to words to words _ _ writ -- ten on sand
      or to bub -- bles which on the _ wa -- ter wa -- ter _ swim
    }
  >>

  Wilt thou be thus a -- bus -- ed still
  see -- ing that she will right thee ne -- ver
  if thou canst not o'er come her will
  thy love will be thus fruit -- less e -- ver
}

BassMusic = \relative c {
  \repeat volta 2 {
    d4 d4. e8 f8 f,4 g8 a8 f8 a8 b8 c4 d4 a2. a4 d8 g,4 c8 f,2 bes4 g4 a4 a4 d2.
  }

  \repeat volta 2 {
    a'4. g8 f8 e8 d4 cis8 d4 d8 c4. f,8 g4 a2. a4 d8 g,4 c8 f,2 g4 a4
    bes4. bes8 a2.
  }

  \repeat volta 2 {
    f8. f16 f8 f4 f8 f4 f8 f4. g8. g16 g8 g4 g8 g4 g8 g4 g8 a8. a16 a8 a8 a4 a4
    a4 a8 a8 d8. e16 f8 g8 e8 e8 d4 d2
  }
}

BassLyrics = \lyricmode {
  <<
    {
      Can she ex -- cuse ex -- cuse my _ wrongs with vir -- tues cloak
      shall I call her good when she proves un -- kind

      No no where _ sha -- dows do for bo__ __ _ dies stand
      thou mayst be a -- bused if thy sight be dim
    }

    \new Lyrics {
      Are those clear fires clear fires which _ va -- nish in to smoke
      must I praise the leaves where no fruit I find

      Cold love is _ like to words writ -- ten _ on sand
      or to bu -- bles which on the wa -- ter swim

    }
  >>

  Wilt thou be thus a -- bus -- ed still
  see -- ing that she will right thee ne -- ver
  if thou canst not o'er come her will
  thy love will be thus fruit -- less e -- ver

}

% PianoRHMusic = {
%   \partCombine
%   << \SopranoMusic>>
%   << \AltoMusic>>
% }

% PianoLHMusic = {
%   \partCombine
%   << \TenorMusic>>
%   << \BassMusic>>
% }

\include "satb.ly"

\markup {
  \column {
    \line { "Was I so base that I might not aspire" }
    \line { "Unto those hight joys which she holds from me" }
    \line { "As they are high so high is my desire" }
    \line { "If she this deny what can granted be" }
    \line { " " }
    \line { "If she will yield to that which reason is" }
    \line { "It is reason's will that love should be just" }
    \line { "Dear make me happy still by granting this" }
    \line { "Or cut of delays if that die I must" }
    \line { " " }
    \line { "Better a thousand times to die" }
    \line { "Then for to live thus still tormented" }
    \line { "Dear but remember it was I" }
    \line { "Who for thy sake did die contented" }
  }
}

