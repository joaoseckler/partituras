\version "2.22.0"

% Usage: \add_mark \music_var \markup {"tralalal"} 1
%
% Adds markup to last note of music_var. 1 means up and -1 means down
% This only works for very simple music sections

add_mark =
#(define-music-function (m markup n)
                        (ly:music? markup? number?)

   (define note (last (ly:music-property m 'elements)))
   (set! (ly:music-property note 'articulations)
         (cons
          (make-music 'TextScriptEvent 'direction n 'text markup)
          (ly:music-property note 'articulations)))

          (display note)
          #{
            $m
          #})

