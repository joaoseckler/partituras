\version "2.24.3"
\include "sinc.ly"

\header {
  title = "Tá chovendo"
  dedication = \markup \italic "Pra Ju"
  composer = "João Seckler"
  tagline = \markup \teeny {
    "Editado com" \italic "lilypond" \normal-text
    "por João Seckler em jun/2024"
  }
}

\paper {
  system-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 3)
       (stretchability . 12))
  markup-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 8)
       (padding . 3)
       (stretchability . 12))
}

\layout {
  \context {
    \ChordNames
      \override VerticalAxisGroup.
        nonstaff-relatedstaff-spacing.padding = #1.5
  }
}


staffglobal = {
  \key d \minor
  \time 2/4
  \set Timing.beamExceptions = #'()
  \set Timing.beatStructure = 1,1
  \set Staff.explicitKeySignatureVisibility = ##(#f #f #t)
  \set Staff.printKeyCancellation = ##f
}

\score {
  <<
    \new ChordNames \chordmode {
      \partial 16*3 s8.
      d2:m |
      g:m |
      a:7 |
      d4:m e:7 |
      a2 |
      b:m |
      e:7 |
      a:7 |
      d:m |
      g:m |
      e4:7 a:7 |
      a:m7.5- d:7 |
      g:m gis:dim |
      d:m bes |
      e:7 a:7 |

      \partial 16*5 d4:m s16 |
      d2:7 |
      g:m |
      c:7 |
      f4 bes:7 |
      es2 |
      c:m |
      d:7 |
      g:m |
      c:7 |
      f |
      e4:m7.5- a:7 |
      a:m7.5- d:7 |
      es2 |
      c:m |
      d:7 |
      g:m |

      \partial 16*3 a8.:7 |
      d4 b:7 |
      e:m a:7 |
      d a:7 |
      d fis:7 |
      b2:m |
      fis:7 |
      g4 gis:m7.5- |
      fis:m cis:7 |
      fis:m b:7 |
      e:m a:7 |
      d fis:7 |
      b:m b:7 |
      e:m cis:m7.5- |
      b:m b:m/a |
      e:7 a:7 |
      d

    }

    \new Staff {
      \staffglobal
      \new Voice \relative c'' {
        \section \sectionLabel \markup \box A
        \partial 16*3 a16 a' a |
        a aes g ges f f, f' f |
        e es d cis d d, d' d |
        cis8. a16 g gis a e |
        f d' c b r e, e' e | \break
        e d cis b cis cis' cis, cis |
        d cis b ais b b' b, cis |
        d8. b16 gis f? e d |
        bes'?4 r16 a a' a | \break
        a aes g ges f f, f' f |
        e es d cis d g f e |
        d d, d' d cis cis, cis' cis |
        es es, es' es d fis, a c | \break
        bes a bes d d, d' cis e |
        d a f f' f, bes d a |
        gis e' e, d' cis c b bes |
        \partial 16*5 a[ d d, d] r \bar "||" \break

        \section \sectionLabel \markup \box B
        d8. bes'16~ 4 |
        r8 a16 gis \sinc a bes f' |
        e8. g,16~ 4 |
        r8 a16 g \sinc aes c f | \break
        c8. bes16~ 8 d, |
        a'8. g16~ 8 c, |
        r8 a'16 gis a8 d |
        r16 g, g' g g,8 r | \break
        d8. bes'16~ 4 |
        r8 a16 gis \sinc a d f |
        e8. g,16~ 4 |
        r8 c16 b \sinc c d a | \break
        es8. d'16~ 8 c |
        c,8. a'16~ 8 g |
        r8 a16 gis a8 d |
        r16 g, g' g g,8 r \bar "||" \pageBreak

        \section \sectionLabel \markup \box C

        \key d \major

        \partial 16*3 a16 e g |
        fis g a b a fis fis' fis, |
        e fis g a g e e' e, |
        fis8. b16~ 8 a |
        d,8. d16 e fis gis ais | \break
        b cis d e d b b' b, |
        ais b  cis d cis fis, fis' fis, |
        g8. cis16 d e fis gis |
        a4 r16 a, eis gis | \break
        fis gis a b a fis fis' fis, |
        e fis g a g e e' e, |
        d8. fis16 gis ais b cis |
        d4 r16 g dis fis |
        e b b' b, r fis' cis e |
        d fis, fis' fis, r e' b d |
        gis,8. fis16 g a b cis |
        d4 r \fine
      }
    }
  >>
  \layout {}
}

